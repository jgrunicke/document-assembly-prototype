import { Formik, Form, Field, FormikHelpers } from "formik";
import React, { useState } from "react";
import { v4 } from "uuid";

interface Props {}

interface AnyDocValues {
  docType: "view" | "edit" | "";
}

/**
 * Component to create a AnyDoc Block
 */
export const CreateAnyDoc: React.FC<Props> = () => {
  const createAnyDocCC = async (values: AnyDocValues, actions: FormikHelpers<AnyDocValues>) => {
    Word.run(async (context) => {
      const range = context.document.getSelection();

      const contentControl = range.insertContentControl();
      // Give a UUID to each Content-Control
      contentControl.title = v4();
      contentControl.tag = v4();
      contentControl.appearance = "BoundingBox";
      contentControl.cannotEdit = values.docType == "view" ? true : false;
      contentControl.cannotDelete = true;

      actions.setSubmitting(false);

      await context.sync();
    }).catch((error) => {
      console.log("Error: " + error);
      if (error instanceof OfficeExtension.Error) {
        console.log("Debug info: " + JSON.stringify(error.debugInfo));
      }
    });
  };

  return (
    <div>
      <h3>Create AnyDoc</h3>
      <Formik
        initialValues={
          {
            docType: "",
          } as AnyDocValues
        }
        onSubmit={(values, actions) => createAnyDocCC(values, actions)}
      >
        <Form>
          <div>
            <Field as="select" name="docType">
              <option value={""}></option>
              <option value={"view"}>Locked</option>
              <option value={"edit"}>Editable</option>
            </Field>
            <button type="submit">Create</button>
          </div>
        </Form>
      </Formik>
    </div>
  );
};
