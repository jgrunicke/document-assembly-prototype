import React from "react";

/**
 * Component to create a merge tag out of a currently selected word in the document.
 */
export const CreateMergeTag: React.FC = () => {
  const createMergeTag = async () => {
    Word.run(async (context) => {
      const range = context.document.getSelection();

      range.load("text");
      return context
        .sync()
        .then(() => {
          const contentControl = range.insertContentControl();
          contentControl.title = range.text;
          contentControl.tag = range.text;
          contentControl.appearance = "BoundingBox";
        })
        .then(context.sync);
    }).catch((error) => {
      console.log("Error: " + error);
      if (error instanceof OfficeExtension.Error) {
        console.log("Debug info: " + JSON.stringify(error.debugInfo));
      }
    });
  };
  return (
    <>
      <h3>Create MergeTag</h3>
      <button onClick={createMergeTag}>MergeTag</button>
    </>
  );
};
