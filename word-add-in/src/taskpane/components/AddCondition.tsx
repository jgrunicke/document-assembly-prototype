import { Field, Form, Formik, useFormikContext } from "formik";
import React, { useState } from "react";
import { getJsonLogicRule, JsonLogicRuleType } from "./JsonLogic";

type State = {
  rules: JsonLogicRuleType[];
};

/**
 * Component to create a condition. Rules can be entered through input fields.
 *  Every rule gets added to a stack of rules that get inserted together.
 */
export const AddCondition: React.FC<{}> = () => {
  const [state, setState] = useState<State>({
    rules: [],
  });

  /**
   * Function to add a rule from the input fields to the stack of rules.
   * @param rule The rule that should be added to the stack of rules.
   */
  const addRule = (rule: Values) => {
    setState({
      rules: [...state.rules, rule as JsonLogicRuleType],
    });
  };

  /**
   * Function to insert all the rules on the stack into the word document.
   */
  const insertCondition = async () => {
    Word.run(async (context) => {
      const text = getJsonLogicRule(state.rules);

      const insertionPoint = context.document.getSelection();

      const paragraph = insertionPoint.insertParagraph(text, Word.InsertLocation.before);
      // Give the paragraph this style so it can be recognized by the parser.
      paragraph.style = "RS_Metatags";

      await context.sync();

      // Empty the stack again after the rules have been inserted into the document.
      setState({
        rules: [],
      });
    }).catch((error) => {
      console.log("Error: " + error);
      if (error instanceof OfficeExtension.Error) {
        console.log("Debug info: " + JSON.stringify(error.debugInfo));
      }
    });
  };

  return (
    <div>
      <h3>Add Condition</h3>

      <div>
        <h4>Conditions:</h4>
        <div>Use 'fixed' or 'floating' for interest types</div>
        <div>
          {state.rules.map((rule, index) => (
            <div key={index}>{`${rule.key} ${rule.operator} ${rule.value}`}</div>
          ))}
        </div>
      </div>

      <Formik
        initialValues={
          {
            key: "",
            operator: "==",
            value: "",
          } as Values
        }
        onSubmit={(values) => addRule(values)}
      >
        <Form>
          <Field name="key" as="select">
            <option value={""}></option>
            <option value={"interestType"}>Interest type</option>
            <option value={"interestMargin"}>Interest margin</option>
          </Field>
          <Field name="operator" as="select">
            <option value={"=="}>=</option>
            <option value={">"}>{">"}</option>
            <option value={"<"}>{"<"}</option>
          </Field>
          <Field name="value"></Field>
          <button type="submit">Add Condition</button>
        </Form>
      </Formik>
      <button onClick={() => insertCondition()}>Insert Condition</button>
    </div>
  );
};

/**
 * Interface for the values coming from the forms. Will be transformed to OperatorType.
 */
interface Values {
  key: string;
  operator: string;
  value: string;
}
