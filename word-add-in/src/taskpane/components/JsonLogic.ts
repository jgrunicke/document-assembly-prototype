/**
 * Creates a string of a JsonLogicRule-Object for the
 *  conditions set for a section, subsection or anydoc element
 * @param Array of conditions
 * @returns String of the JsonLogic Rule-Object
 */
export const getJsonLogicRule = (rules: JsonLogicRuleType[]) => {
  const mappedRules = rules.map((rule) => {
    return { [rule.operator]: [{ var: rule.key }, rule.value] };
  });

  const rule = {
    and: [...mappedRules],
  };

  return JSON.stringify(rule);
};

/**
 * Describes the contents of a JsonLogicRuleStatement.
 */
export type JsonLogicRuleType = {
  key: string;
  operator: OperatorType;
  value: unknown;
};

export type OperatorType = "==" | "<" | ">";
