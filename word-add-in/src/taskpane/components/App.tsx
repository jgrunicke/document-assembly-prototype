import * as React from "react";
import { CreateAnyDoc } from "./CreateAnyDoc";
import { AddCondition } from "./AddCondition";
import { CreateMergeTag } from "./CreateMergeTag";

/* global Word, require */

export const App: React.FC = () => {
  return (
    <div className="py-2 px-5">
      <CreateMergeTag />
      <AddCondition />
      <CreateAnyDoc />
    </div>
  );
};
