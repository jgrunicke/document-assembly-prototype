import subprocess

filename = input('Enter the name of the file (without .docx) that you want to unzip: ')
documentFolderPath = './documents/'

subprocess.call(['rm', '-rf', documentFolderPath + filename])

subprocess.call(['cp', documentFolderPath + filename+'.docx', documentFolderPath + filename + '.zip'])

subprocess.call(['unzip', documentFolderPath +  filename, '-d' , documentFolderPath + filename])

subprocess.call(['rm', documentFolderPath + filename+'.zip'])