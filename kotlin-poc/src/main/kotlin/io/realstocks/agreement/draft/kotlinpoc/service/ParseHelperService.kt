package io.realstocks.agreement.draft.kotlinpoc.service

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.realstocks.agreement.draft.kotlinpoc.config.logger
import io.realstocks.agreement.draft.kotlinpoc.model.*
import io.realstocks.agreement.draft.kotlinpoc.model.Text
import io.realstocks.agreement.draft.kotlinpoc.model.enums.AcceptedStyleBasis
import io.realstocks.agreement.draft.kotlinpoc.model.enums.MarkValue
import io.realstocks.agreement.draft.kotlinpoc.model.enums.Style
import org.docx4j.XmlUtils
import org.docx4j.wml.*
import org.springframework.stereotype.Service
import javax.xml.bind.JAXBElement


/**
 * Helper Service that holds functionality that gets used by
 *  both the template and the agreement parse-service.
 */
@Service
class ParseHelperService {

    /**
     * Parses the shared information of an AnyDoc block such as title, doc and conditionRule of the block.
     *
     * @param sdtBlock The sdtBlock from the Word document that should be parsed.
     *
     * @return The parsed information of the sdtBlock.
     */
    fun parseAnyDoc(sdtBlock: SdtBlock): AnyDocInformations {
        val tag = sdtBlock.sdtPr.tag
        val content = sdtBlock.sdtContent

        val conditionRule = parseConditionRuleOrNull(content.content)
        val parseFirst = when {
            conditionRule != null -> false
            else -> true
        }

        val docContent = parseDocContent(content, parseFirst)
        val doc = Doc(content = docContent)
        return AnyDocInformations(
            title = tag.`val`,
            doc = doc,
            conditionRule = conditionRule,
        )
    }

    /**
     * Parses a sdtBlock from a Word document and transforms it to an EditDoc object.
     * Note: It has to be checked before whether or not the sdtBlock is an EditDoc by checking if it is locked.
     *
     * @param sdtBlock The sdtBlock which should be parsed to an EditDoc object.
     * @return The parsed EditDoc object.
     */
    fun parseEditDoc(sdtBlock: SdtBlock): EditDoc {
        val editDoc = parseAnyDoc(sdtBlock)

        return EditDoc(
            title = editDoc.title,
            doc = editDoc.doc,
            conditionRule = editDoc.conditionRule,
            commentChains = null,
            listContext = null,
        )
    }

    /**
     * Parses a sdtBlock from a Word document and transforms it to an ViewDoc object.
     * Note: It has to be checked before whether or not the sdtBlock is a ViewDoc by checking if it is locked.
     *
     * @param sdtBlock The sdtBlock which should be parsed to a ViewDoc object.
     * @return The parsed ViewDoc object.
     */
    fun parseViewDoc(sdtBlock: SdtBlock): ViewDoc {
        val viewDoc = parseAnyDoc(sdtBlock)

        return ViewDoc(
            title = viewDoc.title,
            doc = viewDoc.doc,
            conditionRule = viewDoc.conditionRule,
        )
    }

    /**
     * Tries to parse a conditionRule
     * @param content The list of elements it should look for the condition rule in
     * @param index The index of the element in the array of elements that can be a condition. If none is given
     *  it just look for the first element in the array.
     * @returns a jsonNode that represents the condition rule in JsonLogic syntax.
     */
    fun parseConditionRuleOrNull(content: List<Any>, index: Int = 0): JsonNode? {
        if (content.isEmpty()) return null

        val element = content[index]

        logger.debug("parseConditionRuleOrNull(): element=${element}, index=${index}")

        if (element !is P) {
            logger.debug("parseConditionRuleOrNull(): Element is no P. element=${element}")
            return null
        }

        val style = parseStyle(element)
        if (style != Style.METATAGS.label) {
            logger.debug("parseConditionRuleOrNull(): No P or wrong Style. Style=${style}")
            return null
        }

        val ruleStr = element.content
            .filterIsInstance<R>()
            .flatMap { it.content }
            .map { XmlUtils.unwrap(it) }
            .filterIsInstance<org.docx4j.wml.Text>()
            .joinToString() { it.value }

        logger.debug("parseConditionRuleOrNull(): ruleStr=${ruleStr}")

        val objectMapper = ObjectMapper()

        return objectMapper.readTree(ruleStr)
    }

    /**
     * Helper function to access the style property of a paragraph object.
     *
     * @return The name of the style of the paragraph or null if none is set.
     */
    private fun parseStyle(p: P): String? {
        return p.pPr?.pStyle?.`val`
    }

    /**
     * Parses the content of a doc.
     *
     * @param sdtContent The content of the sdtblock for which the doc should be parsed.
     * @param parseFirst In a previous step it got check if the first element of the sdtContent is a conditionRule.
     *  If this is the case the function should not parse the first element. Is set to true by default.
     *
     * @return The list of docContents that could be parsed.
     */
    private fun parseDocContent(sdtContent: SdtContent, parseFirst: Boolean = true): MutableList<DocContent> {
        val docContent = mutableListOf<DocContent>()
        val content = sdtContent.content
        var i = when {
            parseFirst -> 0
            else -> 1
        }
        while (i < content.size) {
            when (val element = content[i]) {
                // if the element is a SdtBlock at this stage it has to be a merge tag.
                is SdtBlock -> {
                    val mergeTag = parseMergeTag(element.sdtPr.tag)
                    val paragraph = Paragraph(
                        content = mutableListOf(mergeTag)
                    )
                    docContent.add(paragraph)
                    i++
                }
                is P -> {
                    val paragraphOrListResult = parseParagraphOrList(content, i)
                    val paragraphOrList = paragraphOrListResult.result as DocContent
                    i = paragraphOrListResult.index
                    docContent.add(paragraphOrList)
                }
                else -> i++
            }
        }

        return docContent
    }

    /**
     * Tries to parse a element of a sdtContentList that can be either a paragraph or a list
     *
     * @param content The content list of the sdtBlock that gets parsed.
     * @param i The index of the looked at element in the list.
     *
     * @return The parsed Paragraph or List
     */
    private fun parseParagraphOrList(content: List<Any>, i: Int): Result<*> {
        val p = content[i] as P
        val listStyle = hasListStyle(p)

        return when (listStyle) {
            true -> parseList(content, i, 0)
            else -> {
                val paragraph = parseParagraph(p)
                return Result(
                    result = paragraph,
                    index = i + 1
                )
            }
        }
    }

    /**
     * Parses a paragraph element from a Word document and transforms it into a Paragraph object.
     *
     * @param p The paragraph element from the Word document.
     *
     * @return The parsed Paragraph object.
     */
    private fun parseParagraph(p: P): Paragraph {
        val paragraphContent = mutableListOf<ParagraphContent>()

        p.content.mapNotNull {
            logger.debug("parseParagraph(): content=$it")
            when (it) {
                is R -> parseR(it)
                is JAXBElement<*> -> parseBookmarkOrMergeTag(it)
                else -> null
            }
        }.forEach { paragraphContent.add(it) }

        return Paragraph(content = paragraphContent)
    }

    /**
     * Checks wheter a sdtBlock is locked or not.
     *
     * @param sdt The sdtBlock which is to be checked.
     *
     * @return True if the sdtBlock has the SDT_CONTENT_LOCKED property set. Otherwise false.
     */
    fun getSdtIsLocked(sdt: SdtBlock): Boolean {
        val lock = sdt.sdtPr.rPrOrAliasOrLock
            .filterIsInstance<JAXBElement<Any>>()
            .map { XmlUtils.unwrap(it) }
            .filterIsInstance<CTLock>()
            .map { it.`val` }

        if (STLock.SDT_CONTENT_LOCKED in lock) {
            return true
        }

        return false
    }

    /**
     * Tries to parse a wrapped JAXBElement into a Bookmark or MergeTag.
     *
     * @param element The wrapped JAXBElement.
     *
     * @return A MergeTag a Bookmark or null if the element was neither of them.
     */
    private fun parseBookmarkOrMergeTag(element: JAXBElement<*>): MergeTag? {
        val unwrapped = XmlUtils.unwrap(element)
        return when (unwrapped) {
            is SdtRun -> parseMergeTag(unwrapped.sdtPr.tag)
            // TODO: Parse Bookmark
            else -> null
        }
    }

    /**
     * Parses a Run element of a Word document.
     *
     * @param r The run element
     *
     * @return The parsed Run as Text object.
     */
    private fun parseR(r: R): Text {
        val marks = parseMarks(r)

        val text = r.content.filterIsInstance<JAXBElement<*>>().map { XmlUtils.unwrap(it) }
            .filterIsInstance<org.docx4j.wml.Text>()
            .joinToString { it.value }

        return Text(text = text, marks = marks)
    }

    /**
     * Parses the marks of a run element in a Word document
     *
     * @param r The run which marks should be parsed.
     *
     * @return The list of marks that could be found.
     */
    private fun parseMarks(r: R): List<Mark> {
        val marks = mutableListOf<Mark>()

        val rPr = r.rPr ?: return marks

        if (rPr.b !== null || rPr.bCs !== null) {
            marks.add(Mark(value = MarkValue.STRONG.name))
        }

        if (rPr.i !== null || rPr.iCs !== null) {
            marks.add(Mark(value = MarkValue.EM.name))
        }

        return marks
    }

    /**
     * Parses a Ordered List. This is a recursive function that calls itself it there is a list within the list (
     *  multilevel list).
     *
     * @param content List of contents out of which the list should be parsed out.
     * @param i The current index of the list. This is the first possible element of the list.
     * @param level The level of the list that should be parsed. Increases by recursive calls.
     *
     * @return The parsed OrderedList object.
     */
    private fun parseList(content: List<Any>, i: Int, level: Int): Result<OrderedList> {
        var index = i

        val listContext = parseContext(content[i] as P)
        val listContent = mutableListOf<ListItem>()

        while (index < content.size) {
            val element = content[index]

            if (element !is P) {
                index ++
                continue
            }

            val elementContext = parseContext(element)
            when (true) {
                checkListTerminationCriteria(elementContext, listContext) -> break

                checkNestedListCriteria(elementContext, listContext) -> {
                    val parseOrderedListResult = parseList(content, index, level + 1)
                    index = parseOrderedListResult.index
                    listContent.add(parseOrderedListResult.result)
                }

                checkParagraphListItemCriteria(elementContext) -> {
                    val paragraph = parseParagraph(element)
                    listContent.add(paragraph)
                    index++
                }
                else -> {
                    index++
                }
            }
        }

        val orderedList = OrderedList(listContext = listContext, content = listContent)

        return Result(
            index = index,
            result = orderedList
        )
    }

    /**
     * Checks if the listcontext of an element fits to be a new paragraph within the list that gets parsed.
     *
     * @param elementContext The context of the element of the current iteration.
     * @return True if the context of the element leads to a paragraph within a list.
     */
    private fun checkParagraphListItemCriteria(elementContext: ListContext): Boolean
        = elementContext.style == Style.MULTILEVELLIST.label


    /**
     * Checks if the the element of the current iteration leads to a nested list within the currently parsed list.
     *
     * @param elementContext The context of the element of the current iteration.
     * @param listContext The context of the list that gets currently parsed.
     *
     * @return If the element is paragraph with the needed list style and has the same listLevel as the list that gets
     *  parsed the function returns true. Otherwise false.
     */
    private fun checkNestedListCriteria(elementContext: ListContext, listContext: ListContext): Boolean {
        val listLevel = listContext.level ?: 0

        return (elementContext.style == Style.MULTILEVELLIST.label
            && elementContext.level != null && elementContext.level > listLevel)
    }

    /**
     * Checks if the the element of the current iteration leads to an end of the currently parsed list.
     *
     * @param elementContext The context of the element of the current iteration.
     * @param listContext The context of the currently parsed list.
     *
     * @return True if the current element is not a paragraph with the needed listStyle or not from the same list or
     *  from a lower level. Otherwise false.
     */
    private fun checkListTerminationCriteria(elementContext: ListContext, listContext: ListContext): Boolean {
        return ((elementContext.style != Style.MULTILEVELLIST.label) ||
            ((elementContext.numId != null && elementContext.numId != listContext.numId) ||
                ((elementContext.level != null && listContext.level != null) && elementContext.level < listContext.level)))
    }

    /**
     * Parses the ListContext properties of a Paragraph element in a Word document.
     *
     * @param p The Paragraph element.
     *
     * @return The parsed ListContext object.
     */
    private fun parseContext(p: P): ListContext {
        val properties = p.pPr
        val style = properties.pStyle
        val numberingId = properties?.numPr?.numId
        val level = properties?.numPr?.ilvl?.`val`?.toInt()

        return ListContext(
            style = style.`val`,
            numId = numberingId?.`val`?.toInt(),
            level = level
        )
    }

    /**
     * Checks if a paragraph element has the needed style for a list element.
     *
     * @param p The paragraph element.
     *
     * @return True if the style of the paragraph is equal the list style.
     */
    private fun hasListStyle(p: P): Boolean {
        if (p.pPr == null) return false
        val style = p.pPr.pStyle.`val`
        logger.debug("hasListStyle(): style=${style}")

        if (style.contains(AcceptedStyleBasis.MULTILEVELLIST.label) ) return true

        return false
    }

    /**
     * Parses a MergeTag by transforming a tag to a MergeTag object.
     *
     * @param tag The tag that should be parsed to a MergeTag object.
     * @return The parsed MergeTag object.
     */
    private fun parseMergeTag(tag: Tag): MergeTag {
        logger.debug("parseMergeTag(): Tag=${tag.`val`}")
        val query = calculateJsonLogicRule(tag)

        return MergeTag(
            tag = tag.`val`,
            query = query
        )
    }

    /**
     * Calculates the JsonLogicRule node to access the property of the MergeTag.
     *
     * @param tag The tag for which the node should be calculated.
     * @return The JsonNode with the calculated JsonLogicRule
     */
    private fun calculateJsonLogicRule(tag: Tag): JsonNode {
        val json = "{ \"var\": \"${tag.`val`}\" } "
        logger.debug("CalculateJsonLogicRule():json=${json}")

        val objectMapper = ObjectMapper()

        return objectMapper.readTree(json)
    }

    companion object {
        val logger = logger()
    }
}
