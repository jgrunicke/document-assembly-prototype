package io.realstocks.agreement.draft.kotlinpoc.model

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.JsonNode

/**
 * This class describes the actual structure of an document and can be used to exchange information about it
 * media-independent.
 */
data class Document(
    val type: String = "document",
    val preamble: MutableList<AnyDoc>,
    val sections: MutableList<Section>,

    val commentChainMap: MutableMap<Int, CommentChain>?,
) {
    /**
     * Updates an anydoc in a document object.
     *
     * @param title The title/id of the doc that should be updated.
     * @param doc the updated doc of the anydoc
     */
    fun updateEditDocs(title: String, doc: Doc) {
        // Search for anydocs with the needed title in the preamble.
        this.preamble
            .filterIsInstance<EditDoc>()
            .filter { it.title == title }
            .map { it.doc = doc }

        // Search for anydocs with the needed title in the preambles of all sections
        this.sections.flatMap { it.preamble }
            .filterIsInstance<EditDoc>()
            .filter { it.title == title }
            .map { it.doc = doc }

        // Search for anydocs with the needed title in the content of all subsections
        this.sections.flatMap { it.subsections }.flatMap { it.content }
            .filterIsInstance<EditDoc>()
            .filter { it.title == title }
            .map { it.doc = doc }
    }
}

/**
 * A section of an agreement can have a preamble and several subsections.
 * The condition rule decides whether the section gets displayed according to the current loan configuration.
 */
data class Section(
    val type: String = "section",
    val sectionNumber: String?,
    val heading: Heading,
    val preamble: MutableList<AnyDoc> = mutableListOf(),
    val subsections: MutableList<Subsection> = mutableListOf(),
    val conditionRule: JsonNode?,
)

/**
 * A subsection is the lowest level of sections for this type of document. This might have to be adjusted later.
 * The condition rule decides whether the section gets displayed according to the current loan configuration.
 * The contents of a subsection is list of anydocs objects.
 */
data class Subsection(
    val type: String = "subsection",
    val sectionNumber: String?,
    val heading: Heading,
    val content: MutableList<AnyDoc> = mutableListOf(),
    val conditionRule: JsonNode?,
)

/**
 * Holds information about how the heading of a section or subsesction should be displayed.
 */
data class Heading(
    val type: String = "heading",
    val title: String,
    val level: Int,

    val bookmarks: MutableMap<String, Boolean>?,
)

/**
 * An Anydoc is a block of contents. It can either be a EditDoc or AnyDoc depending on if the block of contents should
 * be editable for the user.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = EditDoc::class, name = "edit_doc"),
    JsonSubTypes.Type(value = ViewDoc::class, name = "view_doc")
)
interface AnyDoc{}

/**
 * A EditDoc is a block of content that can't edited.
 *
 * The content of the ViewDoc is encapsulated in a Doc object.
 *
 * The condition rule decides whether this block of content should be displayed in the document
 *  regarding the current configurations of the loan.
 *
 *  The title is the id of the element that makes it uniquely identifiable in the document.
 */
data class ViewDoc(
    val type: String = "view_doc",
    val doc: Doc,
    val title: String,

    val conditionRule: JsonNode?,
): AnyDoc

/**
 * A EditDoc is a block of content that can't edited.
 *
 * The content of the EditDoc is encapsulated in a Doc object.
 *
 * The condition rule decides whether this block of content should be displayed in the document
 *  regarding the current configurations of the loan.
 *
 * The title is the id of the element that makes it uniquely identifiable in the document.
 */
data class EditDoc(
    val type: String = "edit_doc",
    var doc: Doc,
    val title: String,
    val conditionRule: JsonNode?,
    val commentChains: MutableList<CommentChain>? = mutableListOf(),

    val listContext: ListContext?,

    ): AnyDoc

/**
 * A Doc has a list of different contents.
 */
data class Doc(
    val type: String = "doc",
    var content: MutableList<DocContent> = mutableListOf(),
)

/**
 * A DocContent can be either a OrderedList or a Paragraph
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = OrderedList::class, name = "ordered_list"),
    JsonSubTypes.Type(value = Paragraph::class, name="paragraph")
)
interface DocContent{}

/**
 * A ListItem can be a paragraph if it is a normal bullet point of the list or a OrderedList.
 * The letter case means that the ordered list has a nested list, which means that the paragraphs in that
 * list will get displayed at a lower level.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = OrderedList::class, name = "ordered_list"),
    JsonSubTypes.Type(value = Paragraph::class, name="paragraph")
)
interface ListItem{}

/**
 * An ordered list displays bullet point in a ordered and multilevel way.
 */
data class OrderedList(
    val type: String = "ordered_list",
    val content: MutableList<ListItem>,

    val listContext: ListContext,
) : DocContent, ListItem

/**
 * A Paragraph is a list of texts. It can be displayed as it is or as point in a list.
 */
data class Paragraph(
    val type: String = "paragraph",
    val content: MutableList<ParagraphContent> = mutableListOf(),
): DocContent, ListItem

/**
 * The contents of a paragraph can be Texts or MergeTags
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = MergeTag::class, name = "merge_tag"),
    JsonSubTypes.Type(value = Text::class, name="text")
)
interface ParagraphContent{}

/**
 * A Text describes a string for which the same styling rules comply. The styling rules are described as marks.
 */
data class Text(
    val type: String = "text",
    val text: String,
    val marks: List<Mark>
): ParagraphContent

/**
 * A mark describes extra styling rules for a text. A mark can be e.g. 'strong'.
 */
data class Mark(
    val type: String = "mark",
    val value: String,
)

/**
 * A chain of comments describes the list of comments made for one section in the document.
 */
data class CommentChain(
    val type: String = "comment_chain",
    val comments: MutableList<Comment> = mutableListOf(),
)

/**
 * A single comment saved in a chain.
 */
data class Comment(
    val type: String = "comment",
    val durableId: String,
    val done: Boolean,
    val content: Doc,
)

/**
 * The list context describes the properties of the OOXML paragraph element inside a list.
 * This is important to create a list in a word document that goes beyond multiple anydocs.
 */
data class ListContext(
    val type: String = "list_context",
    val style: String,
    val numId: Int?,
    val level: Int?,
)

/**
 * A MergeTag describes a variable that should get replaced by the value of the equally named property in the
 *  loan configurations.
 */
data class MergeTag(
    val type: String = "merge_tag",
    val tag: String,
    val query: JsonNode,
): ParagraphContent

/**
 * Helper class that holds all the shared informations of Edit and ViewDoc objects.
 */
data class AnyDocInformations(
    var doc: Doc,
    val title: String,
    val conditionRule: JsonNode?,
)
