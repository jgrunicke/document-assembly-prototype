package io.realstocks.agreement.draft.kotlinpoc.model.enums

/**
 * The text of an agreement can have different marks like e.g. 'strong'.
 */
enum class MarkValue(label: String) {
    STRONG("strong"),
    EM("em")
}
