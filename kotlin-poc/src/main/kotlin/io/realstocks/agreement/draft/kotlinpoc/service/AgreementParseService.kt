package io.realstocks.agreement.draft.kotlinpoc.service

import io.realstocks.agreement.draft.kotlinpoc.config.logger
import io.realstocks.agreement.draft.kotlinpoc.model.Document
import io.realstocks.agreement.draft.kotlinpoc.model.EditDoc
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.SdtBlock
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

/**
 * Service provides functionalities to parse a Word document and transform it into a document object.
 */
@Service
class AgreementParseService(
    val helperService: ParseHelperService
) {
    /**
     * Function to parse the changes made to an agreement in Microsoft Word and update the previous version of the
     *  document object.
     *
     * @param document The previous version of the document object of the agreement for which the changes should be
     *  parsed.
     * @param file The Word file with the changes that should be parsed.
     *
     * @return The updated version of the document object.
     */
    fun parse(document: Document, file: MultipartFile): Document {

        val wordPackage = WordprocessingMLPackage.load(file.inputStream)
        val mdp = wordPackage.mainDocumentPart

        val content = mdp.content

        // Only look at the changes made to the editDocs since changes outside of them are not allowed anyways.
        // First all the sdt blocks get "parsed" and afterwards the document object gets updated if a sdt block
        //  with the same tag exists.
        content.filterIsInstance<SdtBlock>()
            .mapNotNull { parseEditDoc(it) }
            .forEach { document.updateEditDocs(it.title, it.doc) }

        return document
    }

    /**
     * Helper function parse an EditDoc block of the Word document.
     *
     * @param sdt The sdt-block which could be an edit doc.
     *
     * @return The parsed EditDoc object or null if the sdt block was no EditDoc block.
     */
    private fun parseEditDoc(sdt: SdtBlock): EditDoc? {
        // Determines whether the SdtBlock is an EditDoc or ViewDoc.
        val locked = helperService.getSdtIsLocked(sdt)

        if (!locked) {
            return helperService.parseEditDoc(sdtBlock = sdt)
        }
        return null
    }

    private companion object {
        val logger = logger()
    }
}
