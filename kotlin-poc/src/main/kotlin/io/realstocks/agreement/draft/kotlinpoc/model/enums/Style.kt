package io.realstocks.agreement.draft.kotlinpoc.model.enums

/**
 * The names of the different styles used in the style template
 */
enum class Style(val label: String) {
    H1("RSHeading1"),
    H2("RSHeading2"),
    MULTILEVELLIST("RSMultilevelList"),
    BODY("RSText"),
    METATAGS("RSMetatags")
}

/**
 * To provide a less error prone parsing of the styles this enum gets used.
 * It holds the key words that have to be in the name of the style that is actually used.
 * If the user accidentally uses "Heading1" instead of "RSHeading1" as style the parser can still find it.
 */
enum class AcceptedStyleBasis(val label: String) {
    H1("Heading1"),
    H2("Heading2"),
    MULTILEVELLIST("MultilevelList"),
    BODY("Text"),
}
