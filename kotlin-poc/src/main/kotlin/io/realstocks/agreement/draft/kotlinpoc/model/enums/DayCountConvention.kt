package io.realstocks.agreement.draft.kotlinpoc.model.enums

/**
 * Enum to desscribe the different possible expresssions of the day count convention for an agreement.
 */
enum class DayCountConvention(val value: String) {
    ACTUAL_360("A/360"),
    ACTUAL_365("A/365"),
    ACTUAL_ACTUAL("A/A"),
    THIRTY_365("30/365")
}
