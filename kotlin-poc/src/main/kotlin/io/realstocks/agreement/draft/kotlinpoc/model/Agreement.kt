package io.realstocks.agreement.draft.kotlinpoc.model

/**
 * This class describes an agreement. An agreement is a proxy object for the real document object that holds
 * additional metadata about it like the loanId, version, properties (of the template).
 */
data class Agreement(
    val loanId: String,
    val version: Int,
    val properties: Map<String, Any>,

    val document: Document,){
}

