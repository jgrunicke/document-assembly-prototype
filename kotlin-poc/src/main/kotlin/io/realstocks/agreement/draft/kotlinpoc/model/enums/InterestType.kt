package io.realstocks.agreement.draft.kotlinpoc.model.enums

/**
 * Enum to describe the possible expressions of the interest type of an agreement.
 */
enum class InterestType(val value: String) {
    floating("floating"),
    fixed("fixed")
}
