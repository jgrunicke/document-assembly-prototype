package io.realstocks.agreement.draft.kotlinpoc.service

import io.realstocks.agreement.draft.kotlinpoc.config.logger
import io.realstocks.agreement.draft.kotlinpoc.model.*
import io.realstocks.agreement.draft.kotlinpoc.model.enums.AcceptedStyleBasis
import org.docx4j.XmlUtils
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.P
import org.docx4j.wml.R
import org.docx4j.wml.SdtBlock
import org.docx4j.wml.Text
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayOutputStream
import java.io.File
import java.nio.charset.StandardCharsets
import java.util.*

/**
 * Service holds functionality to manage templates.
 */
@Service
class TemplateService(
    val helperService: ParseHelperService
) {
    /**
     * Loads and returns the StyleTemplate Word document that gets used to create new templates.
     *
     * @return Map of the string `base64` and the StyleTemplate encoded as base64 string.
     */
    fun getStyleTemplate(): Map<String, String> {
        val file = File(templateFilePath)
        val wordPackage = WordprocessingMLPackage.load(file)

        val output = ByteArrayOutputStream()
        wordPackage.save(output)
        val byteArray = output.toByteArray()
        val base64 = String(Base64.getEncoder().encode(byteArray), StandardCharsets.UTF_8)
        return mapOf<String, String>("base64" to base64)
    }

    /**
     * Parses a Template and creates a new Document object
     *
     * @param file The Microsoft Word file of the new tempalte.
     *
     * @return The created Document object.
     */
    fun parseTemplate(file: MultipartFile): Document {
        logger.debug("TemplateService.parseTemplate()")
        val wordPackage = WordprocessingMLPackage.load(file.inputStream)
        val mdp = wordPackage.mainDocumentPart

        val content = mdp.content

        var index = 0

        val preambleResult = parseAnyDocsList(content, index)
        val preamble = preambleResult.result
        index = preambleResult.index

        val sections = parseSections(content, index)

        return Document(
            preamble = preamble,
            sections = sections,
            commentChainMap = null,
        )
    }

    /**
     * Parses the list of sections in the template.
     *
     * @param content The list of elements in the mainDocumentPart of the template.
     * @param index The index for where the function should start parsing the sections in the list of contents.
     *
     * @return The list of the parsed Sections.
     */
    private fun parseSections(content: List<Any>, index: Int): MutableList<Section> {
        val sections = mutableListOf<Section>()

        var i = index

        while (i < content.size) {
            val element = content[i]
            val heading = parseHeadingOrNull(element)

            if (heading != null && heading.level == 1) {
                val sectionResult = parseSection(content, i+1, heading)
                sections.add(sectionResult.result)
                i = sectionResult.index
            } else {
                i++
            }
        }

        return sections
    }

    /**
     * Parses a section of the template.
     *
     * @param content List of elements in the mainDocumentPart of the template that should be parsed.
     * @param index The index for the list of contents where the section starts.
     * @param heading The heading object of the section that already got parsed.
     *
     * @return The parsed section.
     */
    private fun parseSection(content: List<Any>, index: Int, heading: Heading): Result<Section> {
        var i = index

        val conditionRule = helperService.parseConditionRuleOrNull(content, i)

        if (conditionRule !== null) {
            i++
        }

        val preambleResult = parseAnyDocsList(content, i)
        i = preambleResult.index
        val preamble = preambleResult.result

        val subsectionsresult = parseSubsections(content, i)
        val subsections = subsectionsresult.result
        i = subsectionsresult.index

        val section = Section(
            sectionNumber = null,
            heading = heading,
            preamble = preamble,
            subsections = subsections,
            conditionRule = conditionRule
        )

        return Result(
            result = section,
            index = i,
        )
    }

    /**
     * Parses the subsections of a section.
     *
     * @param content List of elements found in the mainDocumentPart of the template.
     * @param index Index of the list of elements where the subsections start.
     *
     * @return The list of parsed subsection for the section.
     */
    private fun parseSubsections(content: List<Any>, index: Int): Result<MutableList<Subsection>> {
        val subsections = mutableListOf<Subsection>()
        var i = index

        while (i < content.size) {
            val element = content[i]
            val heading = parseHeadingOrNull(element)

            when {
                heading == null -> {
                    i++;
                    continue
                }
                heading.level == 1-> break
                heading.level == 2 -> {
                    val subsectionResult = parseSubsection(content, i+1, heading)
                    i = subsectionResult.index
                    subsections.add(subsectionResult.result)
                }
                else -> i++
            }
        }

        return Result(
            result = subsections,
            index = i
        )
    }

    /**
     * Parses a subsection.
     *
     * @param content List of elements found in the mainDocumentPart of the template.
     * @param index Index of the list of elements where the subsection starts.
     * @param heading The heading object of the section that already got parsed.
     *
     * @return The parsed subsection.
     */
    private fun parseSubsection(content: List<Any>, index: Int, heading: Heading): Result<Subsection> {
        var i = index

        val conditionRule = helperService.parseConditionRuleOrNull(content, i)

        if (conditionRule !== null) {
            i++
        }

        val contentResult = parseAnyDocsList(content, i)
        i = contentResult.index

        return Result(
            index = i,
            result = Subsection(
                sectionNumber = null,
                heading = heading,
                content = contentResult.result,
                conditionRule = conditionRule
            )
        )
    }

    /**
     * Parses a list of EditDocs and ViewDocs.
     *
     * @param content List of elements found in the mainDocumentPart of the template.
     * @param index Index of the list of elements where the list of AnyDocs starts.
     */
    private fun parseAnyDocsList(content: List<Any>, index: Int): Result<MutableList<AnyDoc>> {
        val anyDocsList = mutableListOf<AnyDoc>()

        var i = index

        while (i < content.size) {
            val element = content[i]
            val sdtBlock = getSdtBlockOrNull(element)
            val heading = parseHeadingOrNull(element)

            when {
                (sdtBlock != null) -> {
                    anyDocsList.add(parseAnyDoc(sdtBlock))
                    i++
                }
                (heading != null) -> {
                    break
                }
                else -> i++
            }
        }

        return Result<MutableList<AnyDoc>>(
            index = i,
            result = anyDocsList
        )
    }

    /**
     * Parses one block of contents which can be a EditDoc or ViewDoc.
     *
     * @param sdtBlock The sdtBlock which should be parsed to an EditDoc or ViewDoc.
     *
     * @return The parsed EditDoc or ViewDoc object.
     */
    private fun parseAnyDoc(sdtBlock: SdtBlock): AnyDoc {
        val locked = helperService.getSdtIsLocked(sdtBlock)

        return when (locked) {
            true -> helperService.parseViewDoc(sdtBlock)
            false -> helperService.parseEditDoc(sdtBlock)
        }
    }

    /**
     * Parses an element to a heading object if possible.
     *
     * @param element The element that should be parsed
     * @return The heading object if the element was a Paragraph with the style of a heading or Null.
     */
    private fun parseHeadingOrNull(element: Any): Heading? {
        if (element !is P) return null

        val style = element.pPr?.pStyle?.`val`
        if (style == null) return null

        logger.debug("ParseHeadingOrNull(): element=${element}, style=${style}")

        val level = when {
            style.contains(AcceptedStyleBasis.H1.label) -> 1
            style.contains(AcceptedStyleBasis.H2.label) -> 2
            else -> return null
        }

        val titleStr = element.content
            .filterIsInstance<R>()
            .flatMap { it.content }
            .map { XmlUtils.unwrap(it) }
            .filterIsInstance<Text>()
            .joinToString() { it.value }


        return Heading(title = titleStr, level = level, bookmarks = null)
    }

    /**
     * Tries to cast an unknown element to a sdtBlock.
     *
     * @param element The unknown element.
     *
     * @return The casted element if it is a sdtBlock or Null.
     */
    private fun getSdtBlockOrNull(element: Any): SdtBlock? {
        if (element !is SdtBlock) return null
        return element
    }

    companion object {
        const val templateFilePath = "./documents/Style_Template.docx"
        val logger = logger()
    }
}
