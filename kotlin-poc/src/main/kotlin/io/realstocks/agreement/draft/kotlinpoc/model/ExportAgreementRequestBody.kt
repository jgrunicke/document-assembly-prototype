package io.realstocks.agreement.draft.kotlinpoc.model

/**
 * Request object to create a word document out of an agreement object and the current loan config.
 */
data class ExportAgreementRequest(
    val loanConfig: Map<String, Any>,
    val agreement: Agreement
)
