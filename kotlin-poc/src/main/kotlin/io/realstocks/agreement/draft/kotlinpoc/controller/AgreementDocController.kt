package io.realstocks.agreement.draft.kotlinpoc.controller

import io.realstocks.agreement.draft.kotlinpoc.model.Document
import io.realstocks.agreement.draft.kotlinpoc.model.ExportAgreementRequest
import io.realstocks.agreement.draft.kotlinpoc.service.AgreementParseService
import io.realstocks.agreement.draft.kotlinpoc.service.PrintService
import io.realstocks.agreement.draft.kotlinpoc.service.TemplateService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

/**
 * Controller to map the rest endpoints to service functions.
 */
@RestController
class AgreementDocController(
    val printService: PrintService,
    val parseService: AgreementParseService,
    val templateService: TemplateService
) {
    /**
     * Endpoint to create a word document out of an agreement object.
     *
     * @param requestBody Holds the agreemet object and the current loanconfig.
     * @return Map of the string "base64" and the created word document as a base64 string.
     */
    @CrossOrigin("http://localhost:3333")
    @PostMapping("/document-assembly/print")
    fun printAgreement(@RequestBody requestBody: ExportAgreementRequest): Map<String, String> {
        return printService.printAgreement(requestBody)
    }

    /**
     * Parses the changes made to the an before exported agreement to Microsoft Word.
     * The request body has to be of the MIME-type: multipart/formdata.
     *
     * @param document The previous version of the document of the agreement that got changed.
     * @param file The word document that holds the changes to the agreement.
     * @return The updated document object.
     */
    @CrossOrigin("http://localhost:3333")
    @PostMapping(path=["/document-assembly/parse"], consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun parseAgreement(@RequestPart document: Document, @RequestPart file: MultipartFile): Document {
        return parseService.parse(document, file)
    }

    /**
     * Endpoint to load the style template to create a new template in Microsoft Word.
     *
     * @returns Map of the string "base64" and the word document as a base64 encoded string.
     */
    @CrossOrigin("http://localhost:3333")
    @GetMapping("/document-assembly/style-template")
    fun getStyleTemplate(): Map<String, String> = templateService.getStyleTemplate()

    /**
     * Endpoint to parse a newly created template.
     * Transforms a Word document into a document object.
     * The request body has to be of the MIME-type: multipart/formdata.
     *
     * @param file The microsoft Word file with the new template as an entry in a mulitpart form.
     * @return The parsed template as document object. Can be used to create a new template object.
     */
    @CrossOrigin("http://localhost:3333")
    @PostMapping(path = ["/document-assembly/template"], consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun parseTemplate(@RequestPart file: MultipartFile): Document {
        return templateService.parseTemplate(file)
    }
}
