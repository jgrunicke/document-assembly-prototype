package io.realstocks.agreement.draft.kotlinpoc.model

/**
 * Utility class to return when parsing through all the contents of the main document part of
 *  Word document.
 */
class Result<T>(
    val index: Int,
    val result: T,
)
