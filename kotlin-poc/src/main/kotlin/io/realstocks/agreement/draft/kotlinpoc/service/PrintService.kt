package io.realstocks.agreement.draft.kotlinpoc.service

import io.realstocks.agreement.draft.kotlinpoc.config.logger
import io.realstocks.agreement.draft.kotlinpoc.model.*
import io.realstocks.agreement.draft.kotlinpoc.model.Text
import io.realstocks.agreement.draft.kotlinpoc.model.enums.MarkValue
import io.realstocks.agreement.draft.kotlinpoc.model.enums.Style
import org.docx4j.XmlUtils
import org.docx4j.model.listnumbering.ListNumberingDefinition
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart
import org.docx4j.openpackaging.parts.WordprocessingML.NumberingDefinitionsPart
import org.docx4j.wml.*
import org.docx4j.wml.Numbering.Num
import org.springframework.stereotype.Service
import java.io.ByteArrayOutputStream
import java.io.File
import java.nio.charset.StandardCharsets
import java.util.*


/**
 * Service holds functionalities to create a Word document for a loan agreement.
 */
@Service
class PrintService(
    val conditionEvaluator: ConditionEvaluationService
) {
    /**
     * Handles the intial request to create a Word document.
     *
     * @param requestBody The requestBody of the request. Holds the current configurations of the loan and the
     *  agreement object which holds the information about the document that should be created.
     *
     * @return The created Word document as a map of the string `base64` and the document as a base64 encoded string.
     */
    fun printAgreement(requestBody: ExportAgreementRequest): Map<String, String> {
        val (loanConfig, agreement) = requestBody

        val templateFile = fetchTemplate()
        val template = WordprocessingMLPackage.load(templateFile)
        val mdp = template.mainDocumentPart
        val ndp = mdp.numberingDefinitionsPart

        try {
            printDocument(mdp, loanConfig, agreement,ndp)
        } catch (e: ConditionDefinitionException) {
            // TODO: ExceptionHandling
            logger.error(e.message)
        }

        val output = ByteArrayOutputStream()
        saveFile(template, output)
        val byteArray = output.toByteArray()
        val base64 = String(Base64.getEncoder().encode(byteArray), StandardCharsets.UTF_8)
        return mapOf<String, String>("base64" to base64)
    }

    /**
     * Creates the contents of the document and adds them to the MainDocumentPart.
     *
     * @param mdp The MainDocumentPart of the document that gets created.
     * @param loanConfig The current configurations of the loan.
     * @param agreement The agreement object of the loan.
     * @param ndp The numberingDefinitionsPart of the document that gets created. Gets handed down to deeper function
     *  calls that need the ndp.
     */
    private fun printDocument(mdp: MainDocumentPart, loanConfig: Map<String, Any>, agreement: Agreement, ndp: NumberingDefinitionsPart) {
        val document = agreement.document

        val (_, preamble, sections, _) = document

        // Print preamble
        printAnyDocsList(preamble, mdp, loanConfig, ndp)

        // Print sections
        sections.forEach { printSection(it, mdp, loanConfig, ndp) }
    }

    /**
     * Creates a section in the MainDocumentPart.
     *
     * @param section The section that should be created.
     * @param mdp The MainDocumentPart of the document that gets created.
     * @param loanConfig The current configurations of the loan.
     * @param ndp The numberingDefinitionsPart of the document that gets created. Gets handed down to deeper function
     *  calls that need the ndp.
     */
    private fun printSection(
        section: Section,
        mdp: MainDocumentPart,
        loanConfig: Map<String, Any>,
        ndp: NumberingDefinitionsPart) {
        val (_, _,heading, preamble, subsections, conditionRule) = section

        val inDocument = conditionEvaluator.evaluateRule(conditionRule, loanConfig)
        if (!inDocument) return

        printHeading(heading, mdp)

        printAnyDocsList(preamble, mdp, loanConfig, ndp)

        subsections.forEach { printSubsection(it, mdp, loanConfig, ndp) }
    }

    /**
     * Creates a subsection in the MainDocumentPart.
     *
     * @param subsection The subsection that should be created.
     * @param mdp The MainDocumentPart of the document that gets created.
     * @param loanConfig The current configurations of the loan.
     * @param ndp The numberingDefinitionsPart of the document that gets created. Gets handed down to deeper function
     *  calls that need the ndp.
     */
    private fun printSubsection(subsection: Subsection, mdp: MainDocumentPart, loanConfig: Map<String, Any>, ndp: NumberingDefinitionsPart) {
        val (_, _,heading, content, conditionRule) = subsection

        val inDocument = conditionEvaluator.evaluateRule(conditionRule, loanConfig)
        if (!inDocument) return

        printHeading(heading, mdp)

        // Print content
        printAnyDocsList(content, mdp, loanConfig, ndp)
    }

    /**
     * Creates a heading in the document.
     *
     * @param heading The heading object that should get created in the document.
     * @param mdp The MainDocumentPart of the document that gets created.
     */
    private fun printHeading(heading: Heading, mdp: MainDocumentPart) {
        val (_, title, level, _) = heading

        val style = when (level) {
            1 -> Style.H1
            2 -> Style.H2
            else -> Style.BODY
        }
        val p = createP(style)

        // Print Title
        printText(p, title, null)

        mdp.content.add(p)
    }

    /**
     * Creates text in the document.
     *
     * @param p The paragraph element to which the text should be added.
     * @param text The text that should be added.
     * @param marks The list of marks that should be added to the text.
     */
    private fun printText(p: P, text: String, marks: List<Mark>?) {

        val run = factory.createR()
        p.content.add(run)

        if (marks !== null) {
            printMarks(marks, run)
        }

        val t = factory.createText()
        t.value = text
        val tWrapped = factory.createRT(t)
        run.content.add(tWrapped)
    }

    /**
     * Adds marks to a run element in the Word document.
     *
     * @param marks The list of marks that should be added.
     * @param run The run object to which the marks should be applied to.
     */
    private fun printMarks(marks: List<Mark>, run: R) {
        val runPr = factory.createRPr()
        run.rPr = runPr
        val booleanDefaultTrue = factory.createBooleanDefaultTrue()

        marks.forEach {
            when(it.value) {
                MarkValue.EM.name -> {
                    runPr.i = booleanDefaultTrue
                    runPr.iCs = booleanDefaultTrue
                }
                MarkValue.STRONG.name -> {
                    runPr.b = booleanDefaultTrue
                    runPr.bCs = booleanDefaultTrue
                }
            }
        }

    }

    /**
     * Creates a P(aragraph) object that can be added to the document.
     *
     * @param style The style of the P object that gets created.
     *
     * @return The created P object.
     */
    private fun createP(style: Style): P {
        val p = factory.createP()
        val pProps = factory.createPPr();
        p.pPr = pProps

        val pprBaseStyle = factory.createPPrBasePStyle()
        pprBaseStyle.`val` = style.label
        pProps.pStyle = pprBaseStyle

        return p
    }

    /**
     * Adds a list of AnyDocs to the document.
     *
     * @param anyDocs The list of anydocs that should be added.
     * @param mdp The MainDocumentPart of the document that gets created.
     * @param loanConfig The current configurations of the loan.
     * @param ndp The numberingDefinitionsPart of the document that gets created. Gets handed down to deeper function
     *  calls that need the ndp.
     */
    private fun printAnyDocsList(anyDocs: MutableList<AnyDoc>, mdp: MainDocumentPart, loanConfig: Map<String, Any>, ndp: NumberingDefinitionsPart) {
        anyDocs.forEach {
            when (it::class) {
                EditDoc::class -> printEditDoc((it as EditDoc), mdp, loanConfig, ndp)
                ViewDoc::class -> printViewDoc((it as ViewDoc), mdp, loanConfig, ndp)
            }
        }
    }

    /**
     * Adds a ViewDoc to the document.
     *
     * @param viewDoc The ViewDoc that should be added.
     * @param mdp The MainDocumentPart of the document that gets created.
     * @param loanConfig The current configurations of the loan.
     * @param ndp The numberingDefinitionsPart of the document that gets created. Gets handed down to deeper function
     *  calls that need the ndp.
     */
    private fun printViewDoc(
        viewDoc: ViewDoc,
        mdp: MainDocumentPart,
        loanConfig: Map<String, Any>,
        ndp: NumberingDefinitionsPart
    ) {
        val (_, doc, title, conditionRule) = viewDoc
        if (!conditionEvaluator.evaluateRule(conditionRule, loanConfig)) return

        val sdtBlock = createSdtBlock(title, true)

        printDoc(doc, sdtBlock.sdtContent, loanConfig, ndp)

        mdp.content.add(sdtBlock)

    }

    /**
     * Adds the content of an AnyDoc element to the sdtBlock of the element.
     *
     * @param doc Holds the content.
     * @param mdp The MainDocumentPart of the document that gets created.
     * @param loanConfig The current configurations of the loan.
     * @param ndp The numberingDefinitionsPart of the document that gets created. Gets handed down to deeper function
     *  calls that need the ndp.
     */
    private fun printDoc(doc: Doc, sdtContent: SdtContent, loanConfig: Map<String, Any>, ndp: NumberingDefinitionsPart) {
        val content = doc.content
        content.forEach {
            when(it::class) {
                Paragraph::class -> printParagraph((it as Paragraph), sdtContent, loanConfig)
                OrderedList::class -> printOrderedList((it as OrderedList), sdtContent, loanConfig, ndp)
            }
        }
    }

    /**
     * Creates and adds an OrderedList to the content of a AnyDoc block.
     *
     * @param orderedList The list that should be created and added.
     * @param mdp The MainDocumentPart of the document that gets created.
     * @param loanConfig The current configurations of the loan.
     * @param ndp The numberingDefinitionsPart of the document that gets created. Gets handed down to deeper function
     *  calls that need the ndp.
     */
    private fun printOrderedList(orderedList: OrderedList, sdtContent: SdtContent, loanConfig: Map<String, Any>, ndp: NumberingDefinitionsPart) {
        val (_, content, listContext) = orderedList

        getOrCreateNumbering(listContext, ndp)

        content.forEach {
            when (it) {
                is Paragraph -> sdtContent.content.add(printListParagraph(it, listContext, loanConfig))
                is OrderedList -> printOrderedList(it, sdtContent, loanConfig, ndp)
            }
        }
    }

    /**
     * If the to be printed list has a context that is not in the template, the numberingDefinition has to be created
     *  and added to the numberingDefinitionsPart of the document.
     *
     * @param listContext The context of the list that should be created.
     * @param ndp The numberingDefinitonsPart of the document (template).
     */
    private fun getOrCreateNumbering(listContext: ListContext, ndp: NumberingDefinitionsPart) {
        logger.debug("getOrCreateNumbering(): listContext=${listContext}")

        val numId = listContext.numId ?: return

        // Inserts the xml into the Numbering DefinitionsPart
        //  This way is easier since the numberingDefinition is always the same.
        val openXML = ("<w:num w:numId=\"${numId}\" xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\">"
            + "<w:abstractNumId w:val=\"24\"/>"
            + "<w:lvlOverride w:ilvl=\"0\">"
            + "<w:startOverride w:val=\"1\"/>"
            + "</w:lvlOverride>"
            + "<w:lvlOverride w:ilvl=\"1\">"
            + "<w:startOverride w:val=\"1\"/>"
            + "</w:lvlOverride>"
            + "<w:lvlOverride w:ilvl=\"2\">"
            + "<w:startOverride w:val=\"1\"/>"
            + "</w:lvlOverride>"
            + "<w:lvlOverride w:ilvl=\"3\">"
            + "<w:startOverride w:val=\"1\"/>"
            + "</w:lvlOverride>"
            + "<w:lvlOverride w:ilvl=\"4\">"
            + "<w:startOverride w:val=\"1\"/>"
            + "</w:lvlOverride>"
            + "<w:lvlOverride w:ilvl=\"5\">"
            + "<w:startOverride w:val=\"1\"/>"
            + "</w:lvlOverride>"
            + "<w:lvlOverride w:ilvl=\"6\">"
            + "<w:startOverride w:val=\"1\"/>"
            + "</w:lvlOverride>"
            + "<w:lvlOverride w:ilvl=\"7\">"
            + "<w:startOverride w:val=\"1\"/>"
            + "</w:lvlOverride>"
            + "<w:lvlOverride w:ilvl=\"8\">"
            + "<w:startOverride w:val=\"1\"/>"
            + "</w:lvlOverride>"
            + "</w:num>")

        val newNum = XmlUtils.unmarshalString(openXML) as Num


        // Add it to the jaxb object and the hashmap
        (ndp.jaxbElement as Numbering).num.add(newNum)
        val listDef = ListNumberingDefinition(newNum, ndp.abstractListDefinitions)
        ndp.instanceListDefinitions[listDef.listNumberId] = listDef

    }

    /**
     * Creates a paragraph that can be added to a list.
     *
     * @param paragraph The paragraph that should be created.
     * @param listContext The context of the list to which the paragraph should be added to.
     * @param loanConfig The current configurations of the loan.
     *
     * @return The created P(aragraph).
     */
    private fun printListParagraph(paragraph: Paragraph, listContext: ListContext, loanConfig: Map<String, Any>): P {
        val p = createP(Style.MULTILEVELLIST)

        setListParagraphProperties(p, listContext)

        printParagraphContent(paragraph, p, loanConfig)

        return p
    }

    /**
     * Creates the content of a paragraph.
     *
     * @param paragraph The paragraph for which the content should be created.
     * @param p The P element to which the content should be added.
     * @param loanConfig The current configuration of the loan.
     */
    private fun printParagraphContent(paragraph: Paragraph, p: P, loanConfig: Map<String, Any>) {
        val content = paragraph.content
        content.forEach {
            when (it::class) {
                MergeTag::class -> printMergeTag((it as MergeTag), p, loanConfig)
                Text::class -> {
                    val (_, text, marks) = (it as Text)
                    printText(p, text, marks)
                }
            }
        }
    }

    /**
     * Sets the properties for a paragraph that is part of a list.
     *
     * @param p The P(aragraph) element for which the properties should be set for.
     * @param listContext Holds the properties of the list that get set for the paragraph.
     */
    private fun setListParagraphProperties(p: P, listContext: ListContext) {
        val numPr = factory.createPPrBaseNumPr()
        p.pPr.numPr = numPr

        val numlvlPr = factory.createPPrBaseNumPrIlvl()
        numPr.ilvl = numlvlPr
        numlvlPr.`val` = (listContext.level ?: 0).toBigInteger()

        if (listContext.numId != null) {
            val numId = factory.createPPrBaseNumPrNumId()
            numPr.numId = numId
            numId.`val` = listContext.numId.toBigInteger()
        }
    }

    /**
     * Creates and adds a paragraph object to the content of a AnyDoc block.
     *
     * @param paragraph The paragraph that should get created in the sdtBlock.
     * @param sdtContent The list of contents of a sdtBlock.
     * @param loanConfig The current configurations of the loan.
     */
    private fun printParagraph(paragraph: Paragraph, sdtContent: SdtContent, loanConfig: Map<String, Any>) {
        val content = paragraph.content

        val p = createP(Style.BODY)

        content.forEach {
            when (it::class) {
                MergeTag::class -> printMergeTag((it as MergeTag), p, loanConfig)
                Text::class -> {
                    val (_, text, marks) = (it as Text)
                    printText(p, text, marks)
                }
            }
        }

        sdtContent.content.add(p)
    }

    /**
     * Creates and adds a mergeTag to a P(aragraph) element.
     *
     * @param mergeTag The mergeTag that should get created.
     * @param p The P element the mergeTag gets added to.
     * @param loanConfig The current configurations of the loan.
     */
    private fun printMergeTag(mergeTag: MergeTag, p: P, loanConfig: Map<String, Any>) {

        val sdtRun = createInlineSdt(mergeTag.tag)
        val sdtRunWrapped = factory.createPSdt(sdtRun)

        p.content.add(sdtRunWrapped)

        val text = conditionEvaluator.evaluateQuery(mergeTag.query, loanConfig)

        val run = factory. createR()
        sdtRun.sdtContent.content.add(run)

        val t = factory.createText()
        t.value = text
        val tWrapped = factory.createRT(t)
        run.content.add(tWrapped)

        val spacing = createSpacing()
        sdtRun.sdtContent.content.add(index = sdtRun.sdtContent.content.size-1, spacing)
        sdtRun.sdtContent.content.add(spacing)

    }

    /**
     * Creates a spacing after the MergeTag. Otherwise there would be no spacing between the MergeTag and the text
     *  that comes after it.
     *
     * @return The Run that contains the spacing.
     */
    private fun createSpacing(): R {
        val run = factory.createR()

        val spacing = factory.createText()
        val spacingWrapped = factory.createRT(spacing)
        spacing.value = " "
        spacing.space = "preserve"

        run.content.add(spacingWrapped)

        return run
    }

    /**
     * Creates an InlineSdt in the document which gets used as a mergeTag.
     *
     * @param alias The alias of the mergeTag that is visible in the document and the id to read from it again when
     *  parsing.
     *
     * @return The created MergeTag as SdtRun element.
     */
    private fun createInlineSdt(alias: String): SdtRun {
        val sdtRun = factory.createSdtRun()

        val sdtpr = factory.createSdtPr()
        sdtRun.sdtPr = sdtpr

        val tag = factory.createTag()
        sdtpr.tag = tag
        tag.`val` = alias

        val sdtalias = factory.createSdtPrAlias()
        val sdtaliasWrapped = factory.createSdtPrAlias(sdtalias)
        sdtpr.rPrOrAliasOrLock.add(sdtaliasWrapped)
        sdtalias.`val` = alias

        val lock = factory.createCTLock()
        val lockWrapped = factory.createSdtPrLock(lock)
        sdtpr.rPrOrAliasOrLock.add(lockWrapped)
        lock.`val` = STLock.SDT_CONTENT_LOCKED

        val sdtContentRow = factory.createCTSdtContentRun()
        sdtRun.sdtContent = sdtContentRow

        return sdtRun
    }

    /**
     * Creates a SdtBlock Element. This serves as a shell for an AnyDoc block.
     *
     * @param title The ID and Alias that can be seen in the document of the block.
     * @param locked Determines if the block should be locked from editing.
     *
     * @return The created SdtBlock
     */
    private fun createSdtBlock(title: String, locked: Boolean): SdtBlock {
        val sdtBlock = factory.createSdtBlock()
        val sdtpr = factory.createSdtPr()
        sdtBlock.sdtPr = sdtpr

        val tag = factory.createTag()
        tag.`val` = title
        sdtpr.tag = tag

        val alias = factory.createSdtPrAlias()
        alias.`val` = title
        val aliasWrapped = factory.createSdtPrAlias(alias)
        sdtpr.rPrOrAliasOrLock.add(aliasWrapped)

        val lock = factory.createCTLock()
        val lockWrapped = factory.createSdtPrLock(lock)
        sdtpr.rPrOrAliasOrLock.add(lockWrapped)
        lock.`val` = if (locked) {
            STLock.SDT_CONTENT_LOCKED
        } else {
            STLock.SDT_LOCKED
        }

        val sdtcontentBlock = factory.createSdtContentBlock()
        sdtBlock.sdtContent = sdtcontentBlock

        return sdtBlock
    }

    /**
     * Creates and adds an EditDoc Block to the document.
     *
     * @param editDoc The editDoc that should get created.
     * @param mdp The mainDocumentPart of the document to which the editDoc should be added to.
     * @param loanConfig The current configurations of the loann.
     * @param ndp The numberingDefinitionsPart of the document.
     */
    private fun printEditDoc(
        editDoc: EditDoc,
        mdp: MainDocumentPart,
        loanConfig: Map<String, Any>,
        ndp: NumberingDefinitionsPart
    ) {
        val (_, doc, title, conditionRule, commentChains, listContext) = editDoc

        // TODO: create Comments, listItemContext, listContext

        if (!conditionEvaluator.evaluateRule(conditionRule, loanConfig)) return

        val sdtBlock = createSdtBlock(title, false)

        printDoc(doc, sdtBlock.sdtContent, loanConfig, ndp)

        sdtBlock.sdtContent.content.filterIsInstance<P>().forEach {
            logger.debug("Setting Shading")
            val pPr = it.pPr
            val shading = factory.createCTShd()
            pPr.shd = shading

            shading.`val` = STShd.CLEAR
            shading.color = shadingColor
            shading.fill =  shadingFill
        }

        mdp.content.add(sdtBlock)
    }

    /**
     * Saves the WordPrcoessingMLPackage to the OutputStream.
     *
     * @param template The WordProcessingMlPackage that got filled with the contents of the agreement.
     * @param outputStream The ByteArray that gets used to send the file to the frontend.
     */
    private fun saveFile(template: WordprocessingMLPackage, outputStream: ByteArrayOutputStream) {
        template.save(outputStream)
    }

    /**
     * Loads the template file.
     *
     * @return The file of the tempalte.
     */
    private fun fetchTemplate(): File {
        // TODO("Load from S3-Bucket")

        return File(templateFilePath)
    }

    private companion object {
        val logger = logger()
        val factory = ObjectFactory()

        const val templateFilePath = "./documents/Style_Template.docx"

        const val shadingColor = "auto"
        const val shadingFill = "DEE3E6"
    }
}
