package io.realstocks.agreement.draft.kotlinpoc.model.enums

/**
 * Enum to describe the possible expressions of the interest period alignment of a loan.
 */
enum class PeriodAlignment(val value: String) {
    UTILISATION_DATE_ALIGNED("UDA"),
    CALENDAR_ALIGNED("CA"),
    CUSTOM_DATE_Aligned("CDA"),
}
