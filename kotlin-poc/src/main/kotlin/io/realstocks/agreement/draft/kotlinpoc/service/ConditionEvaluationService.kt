package io.realstocks.agreement.draft.kotlinpoc.service

import com.fasterxml.jackson.databind.JsonNode
import io.github.jamsesso.jsonlogic.JsonLogic
import io.realstocks.agreement.draft.kotlinpoc.config.logger
import io.realstocks.agreement.draft.kotlinpoc.model.ConditionDefinitionException
import org.springframework.stereotype.Service

/**
 * This service handles the evaluation of JsonLogic statements for condition rules and queries.
 */
@Service
class ConditionEvaluationService() {
    /**
     * Function to evaluate a condition rule of a section, subsection or anydoc.
     * @param ruleExpression The condition rule in the JsonLogic format as a JsonNode.
     * @param loan The current configuration of the loan which is the data the JsonLogic rule tries to evaluate.
     * @return If the ruleExpression is Null the function returns true. Otherwise it checks if the given rule is valid
     *   with the current loan configuration.
     */
    fun evaluateRule(ruleExpression: JsonNode?, loan: Map<String, Any>): Boolean {
        logger.debug("evaluateRule(): ruleExpression=${ruleExpression.toString()}")
        val ruleAsString = ruleExpression.toString()

        if (ruleAsString == "null") return true

        logger.debug("evaluateRule(): ruleAsString=  $ruleAsString")

        val result = jsonLogic.apply(ruleAsString, loan)
        if (result::class != Boolean::class)
            throw ConditionDefinitionException("Unvalid conditionRule: $ruleAsString, result=${result.toString()}")

        logger.debug("ConditionEvaluationService.evaluateRule(): result=${result.toString()}")
        return result as Boolean

    }

    /**
     * Evaluates the value of the query of a merge tag.
     * @param query The query in the JsonLogic format as JsonNode
     * @param loanConfig The current configuration of the loan that holds the query tries to read the data from.
     * @return The value of the query as a string.
     *
     */
    fun evaluateQuery(query: JsonNode, loanConfig: Map<String, Any>): String {
        val queryAsString = query.toString()
        logger.debug("ConditionEvaluationService.evaluateQuery(): queryString=${queryAsString}")
        val result = jsonLogic.apply(queryAsString, loanConfig)
        if (result == null) return "not Defined"

        logger.debug("ConditionEvaluationService.evaluateQuery(): result=${result.toString()}")
        if (result::class == String::class) return result as String

        return result.toString()
    }

    private companion object {
        val logger = logger()
        val jsonLogic = JsonLogic()
    }
}

class TestClass(
    val interest: String
)
