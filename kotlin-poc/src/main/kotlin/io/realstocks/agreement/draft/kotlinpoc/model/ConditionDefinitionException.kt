package io.realstocks.agreement.draft.kotlinpoc.model

import java.lang.RuntimeException

/**
 * Exception gets thrown when an invalid condition rule gets evaluated. That means that
 *  the template must have been broken or parsed wrongly.
 */
class ConditionDefinitionException(msg: String):  RuntimeException(msg)
