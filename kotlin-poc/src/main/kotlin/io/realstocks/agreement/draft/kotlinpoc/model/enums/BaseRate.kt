package io.realstocks.agreement.draft.kotlinpoc.model.enums

/**
 * Enum to describe the different possible expressssions of the base rate.
 */
enum class BaseRate(val value:String) {
    EURIBOR("EURIBOR"),
    STIBOR("STIBOR"),
    NIBOR("NIBOR"),
    LIBOR("LIBOR"),
}
