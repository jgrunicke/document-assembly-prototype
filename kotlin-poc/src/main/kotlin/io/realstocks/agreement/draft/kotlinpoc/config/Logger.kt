package io.realstocks.agreement.draft.kotlinpoc.config

import org.slf4j.Logger
import org.slf4j.LoggerFactory.getLogger

/**
 * Logger that can be used in all classes and functions.
 */
fun Any.logger(): Logger {
    val clazz = if (this::class.isCompanion) javaClass.enclosingClass else javaClass
    return getLogger(clazz)
}
