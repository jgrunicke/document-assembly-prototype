package io.realstocks.agreement.draft.kotlinpoc.model.enums

/**
 * Enum to describe the possible expressions of the interest period of a loan.
 */
enum class InterestPeriod(val value: String) {
    MONTHLY("Monthly"),
    QUARTERLY("Quarterly"),
    SEMIYEARLY("Semi-yearly"),
    YEARLY("Yearly"),
}
