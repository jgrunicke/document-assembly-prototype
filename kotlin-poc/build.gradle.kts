import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.6.0"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.6.0"
	kotlin("plugin.spring") version "1.6.0"
}

group = "io.realstocks.agreement.draft"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("javax.persistence:javax.persistence-api:2.2")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.docx4j:docx4j-JAXB-ReferenceImpl:11.2.9")
	implementation("javax.xml.bind:jaxb-api")
	implementation("org.glassfish.jaxb:jaxb-runtime")
	implementation("com.google.code.gson:gson")
  implementation("io.github.jamsesso:json-logic-java:1.0.7")


	testImplementation("org.springframework.boot:spring-boot-starter-test")

	runtimeOnly("org.jetbrains.kotlin:kotlin-reflect:1.6.10")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
