service: document-assembly

frameworkVersion: '2'

provider:
  name: aws
  runtime: nodejs12.x
  lambdaHashingVersion: 20201221
  region: eu-west-1
  environment:
    agreementTableName: ${self:custom.agreementTableName}
    templateTableName: ${self:custom.templateTableName}
  iamRoleStatements:
    - Effect: Allow
      Action:
        - dynamodb:*
      Resource: '*'

plugins:
  - serverless-plugin-typescript
  - serverless-dynamodb-local
  - serverless-offline

custom:
  agreementTableName: agreements
  templateTableName: templates
  dynamodb:
    stages:
      - dev
    start:
      port: 8000
      inMemory: true
      migrate: true
      seed: true
    migration:
      dir: offline/migrations
    seed:
      domain:
        sources:
          - table: ${self:custom.templateTableName}
            sources: [./offline/seeds/templates/template1.json, ./offline/seeds/templates/template2.json]

functions:
  createOrGetAgreement:
    handler: src/handler.createOrGetAgreement
    events:
      - httpApi:
          path: /document-assembly/agreement/{loanId}
          method: post
  updateAgreement:
    handler: src/handler.updateAgreement
    events:
      - httpApi:
          path: /document-assembly/agreement/{loanId}
          method: put
  createTemplate:
    handler: src/handler.createTemplate
    events:
      - httpApi:
          path: /document-assembly/template
          method: post
  getTemplates:
    handler: src/handler.getTemplates
    events:
      - httpApi:
          path: /document-assembly/templates
          method: get

resources:
  Resources:
    AgreementTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: ${self:custom.agreementTableName}
        AttributeDefinitions:
          - AttributeName: loanId
            AttributeType: S
          - AttributeName: version
            AttributeType: N
        KeySchema:
          - AttributeName: loanId
            KeyType: HASH
          - AttributeName: version
            KeyType: RANGE
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
        BillingMode: PAY_PER_REQUEST
    TemplateTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: ${self:custom.templateTableName}
        AttributeDefinitions:
          - AttributeName: id
            AttributeType: S
        KeySchema:
          - AttributeName: id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
        BillingMode: PAY_PER_REQUEST
