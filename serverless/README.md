### Preinstallment

`yarn install`

If DynamoDB should be used locally:

`sls dynamodb install`

### Run the project

`sls offline start`

### Transition to no offline

Change dynamodb options in constants/dynamo.ts
