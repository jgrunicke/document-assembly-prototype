import * as yup from 'yup';
import { string } from 'yup/lib/locale';

// Schemas to evaluate the given objects in the request bodies.

export const agreementSchema = yup.object().shape({
  loanId: yup.string(),
  properties: yup.object().required(),
  version: yup.number().required(),
  document: yup.object().required(),
});

// TODO: Add more to the schema
export const loanConfigSchema = yup.object().shape({
  loanId: yup.string().required(),
});

export const templateSchema = yup.object().shape({
  id: yup.string(),
  properties: yup.object(),
  document: yup.object().required(),
});
