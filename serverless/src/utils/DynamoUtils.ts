import * as AWS from 'aws-sdk';

//  This file collects helper functionalities for the crud operations on the dynamoDB instance.

const docClient = new AWS.DynamoDB.DocumentClient({
  region: 'localhost',
  endpoint: 'http://localhost:8000',
});

export const query = async (p: { tableName: string; attributeName: string; attributeValue: string }) => {
  const params = {
    TableName: p.tableName,
    KeyConditionExpression: p.attributeName + ' = :value',
    ExpressionAttributeValues: {
      ':value': p.attributeValue,
    },
  };

  const response = await docClient.query(params).promise();

  return response;
};

export const put = async (p: { tableName: string; item: any }) => {
  return docClient
    .put({
      TableName: p.tableName,
      Item: p.item,
    })
    .promise();
};

export const scan = async (tableName: string) => {
  return docClient
    .scan({
      TableName: tableName,
    })
    .promise();
};
