import * as yup from 'yup';

import { headers } from '../constants/constants';

// This file contains errors and a function to handle the different erros that can occur in the lamdas.

export class HttpError extends Error {
  constructor(public statusCode: number, body: Record<string, unknown> = {}) {
    super(JSON.stringify(body));
  }
}

/**
 * Function to handle the erros in the lambdas.
 * @param e The error that should be handled
 * @returns The response for the lambda according to the error.
 */
export const handleError = (e: unknown) => {
  if (e instanceof yup.ValidationError) {
    console.log('validationError');
    return {
      statusCode: 400,
      headers,
      body: JSON.stringify({
        errors: e.errors,
      }),
    };
  }

  if (e instanceof SyntaxError) {
    return {
      statusCode: 400,
      headers,
      body: JSON.stringify({ error: `invalid request body format : "${e.message}"` }),
    };
  }

  if (e instanceof HttpError) {
    return {
      statusCode: e.statusCode,
      headers,
      body: e.message,
    };
  }

  throw e;
};
