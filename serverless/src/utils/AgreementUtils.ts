import { agreementsTable, templatesTable } from '../constants/constants';
import { AgreementType } from '../types/AgreementType';
import { TemplateType } from '../types/TemplateType';
import { put, query, scan } from './DynamoUtils';
import { HttpError } from './Errors';
import { v4 } from 'uuid';

/**
 * Checks whether the properties of a template or agreement comply with the current configuration of the loan.
 * @param properties The properties of the template or of the template the agreement is based on.
 * @param loanConfig The current configurations of the loan.
 * @returns True if the properties of the template comply with the current configurations of the loan. Otherwise false.
 */
export const checkProperties = (properties: Record<string, unknown>, loanConfig: Record<string, unknown>) => {
  let response = true;
  console.log(`checkProperties(), properties=${JSON.stringify(properties)}, loanConfig=${JSON.stringify(loanConfig)}`);

  Object.keys(properties).forEach(k => {
    response = loanConfig[k] === properties[k] ? true : false;
  });

  return response;
};

/**
 * Loads all the agreements out of the database with a specific loanId.
 * @param loanId The loanId the agreements are queried with.
 * @returns Array of the found agreements.
 */
export const fetchAgreementsByLoanId = async (loanId: string) => {
  const params = {
    tableName: agreementsTable,
    attributeName: 'loanId',
    attributeValue: loanId,
  };

  const response = await query(params);

  if (!response.Items) {
    throw new HttpError(404, { error: 'agreements not found' });
  }

  return response.Items.map(item => item as AgreementType);
};

/**
 * Loads all the temlates in the database.
 * @returns Array of all templates in the database.
 */
export const fetchTemplates = async () => {
  const response = await scan(templatesTable);

  if (!response.Items) throw new HttpError(404, { error: "Templates couldn't be found" });

  return response.Items;
};

/**
 * Tries to find a template that is fitting for the current configurations of the loan.
 * @param loanConfig The current configuration of the loan.
 * @returns The first template in the DB that fits the current loan configs.
 */
export const getFittingTemplate = async (loanConfig: Record<string, unknown>) => {
  const templates = await fetchTemplates();

  const fittingTemplate = templates.find(template => checkProperties(template.properties, loanConfig));

  if (fittingTemplate === undefined) {
    throw new HttpError(404, { error: 'Template not found' });
  }

  return fittingTemplate;
};

/**
 * Saves a new agreement in the database by searching for the fitting template
 *  first and then creating an agreement object based on it.
 * @param loanConfig The current configurations of the loan.
 * @returns The created and saved agreement object.
 */
export const createNewAgreement = async (loanConfig: Record<string, unknown>) => {
  const fittingTemplate = await getFittingTemplate(loanConfig);

  const newAgreement: AgreementType = {
    version: 1,
    loanId: loanConfig.loanId as string,
    properties: fittingTemplate.properties,
    document: fittingTemplate.document,
  };

  await put({ tableName: agreementsTable, item: newAgreement });

  return newAgreement;
};

/**
 * Helper function to find the newest version out of an array of agreements.
 * @param agreements Array of agreements of which the newest version is searched for.
 * @returns The agreement with the newest version.
 */
export const getNewestVersion = (agreements: AgreementType[]) => {
  return agreements.sort((a, b) => a.version - b.version).pop();
};
