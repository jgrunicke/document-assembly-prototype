const offlineOptions = {
  region: 'localhost',
  endpoint: 'http://localhost:8000',
};

const options = {};

// set to options when not used in offline
export const dynamoDbOptions = offlineOptions;
