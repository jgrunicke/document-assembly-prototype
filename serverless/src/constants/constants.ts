export const headers = {
  'content-type': 'application/json',
};

export const agreementsTable = 'agreements';

export const templatesTable = 'templates';
