import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { GetAgreementRequestBody } from './types/Requests';
import { v4 } from 'uuid';
import { AgreementDocumentType, AgreementType } from './types/AgreementType';
import { headers, agreementsTable, templatesTable } from './constants/constants';
import { put } from './utils/DynamoUtils';
import { handleError } from './utils/Errors';
import {
  fetchAgreementsByLoanId,
  fetchTemplates,
  checkProperties,
  getFittingTemplate,
  createNewAgreement,
  getNewestVersion,
} from './utils/AgreementUtils';
import { agreementSchema, loanConfigSchema, templateSchema } from './utils/YupSchemas';

/**
 * This endpoints contains some deeper business logic. It is used by the frontend to load the agreements for a configured loan.
 *  If there are no agreements yet in the database with the given loanId, the function creates a new agreement by searching
 *  for a fitting template first. It copies the template and creates a new agreement based on it in the database. If the database holds one or more
 *  agreements for the given loanId the function first checks if the tempalte of the latest version still fitts the current loanConfigs.
 *  If not the function swaps out the templates.
 * @param event The event object that triggers the lambda. It holds the loanId as path parameter and the loan config as request body.
 * @returns An array of agreement objects with the given loanId.
 */
export const createOrGetAgreement = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  console.log('createOrGetAgreement(): event=', event);

  try {
    const loanId = event.pathParameters?.loanId as string;

    const reqBody: GetAgreementRequestBody = JSON.parse(event.body as string);

    loanConfigSchema.validate(reqBody, { abortEarly: false });

    const agreements = await fetchAgreementsByLoanId(loanId);

    console.log('Found agreements: ', agreements);

    let response: AgreementType[] = [];
    let statusCode = 200;

    if (!(agreements.length > 0)) {
      console.log('No agreements found. Creation of new one');

      const newAgreement = await createNewAgreement(reqBody);

      statusCode = 201;
      response.push(newAgreement);
    } else {
      response = [...agreements];

      const newestVersionAgreement = getNewestVersion(agreements);

      if (!checkProperties(newestVersionAgreement.properties, reqBody)) {
        console.log("Properties don't comply anymore. Swapping to new template");

        const template = await getFittingTemplate(reqBody);

        const newAgreement = {
          ...newestVersionAgreement,
          version: newestVersionAgreement.version + 1,
          document: template.document,
          properties: template.properties,
        };

        await put({ tableName: agreementsTable, item: newAgreement });

        statusCode = 201;
        response.push(newAgreement);
      } else {
        console.log('Agreements found and last version complies with properties');
      }
    }

    return {
      statusCode: statusCode,
      headers,
      body: JSON.stringify(response),
    };
  } catch (e) {
    return handleError(e);
  }
};

/**
 * Function to update an agreement after it has been changed on the platform or a new version has been imported from Microsoft Word.
 * @param event Event that triggers the lambda. It holds the loanId as path parameter and the updated version as request body.
 * @returns The updated version of the agreement.
 */
export const updateAgreement = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const loanId = event.pathParameters?.loanId as string;

    const agreements = await fetchAgreementsByLoanId(loanId);
    const newestVersion = getNewestVersion(agreements);

    const reqBody: AgreementDocumentType = JSON.parse(event.body as string);

    const agreement = {
      ...newestVersion,
      version: newestVersion.version + 1,
      document: reqBody,
    };

    await put({ tableName: agreementsTable, item: agreement });

    return {
      statusCode: 201,
      headers,
      body: JSON.stringify(agreement),
    };
  } catch (e) {
    return handleError(e);
  }
};

/**
 * Function to load all templates in the database.
 * @param event
 * @returns The array of templates found in the database.
 */
export const getTemplates = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const response = await fetchTemplates();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify(response),
    };
  } catch (e) {
    return handleError(e);
  }
};

/**
 * Function to save a new template in the database.
 * @param event The event that triggers the lambda. It holds the to be saved template as request body.
 * @returns The saved template object in the database with the newly created UUID.
 */
export const createTemplate = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const reqBody = JSON.parse(event.body as string);

    templateSchema.validate(reqBody, { abortEarly: false });

    const template = {
      ...reqBody,
      id: v4(),
    };

    await put({ tableName: templatesTable, item: template });

    return {
      statusCode: 201,
      headers,
      body: JSON.stringify(template),
    };
  } catch (e) {
    return handleError(e);
  }
};
