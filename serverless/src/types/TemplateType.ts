import { AgreementDocumentType } from './AgreementType';

export type TemplateType = {
  id?: string;
  properties: Record<string, unknown>;
  document: AgreementDocumentType;
};
