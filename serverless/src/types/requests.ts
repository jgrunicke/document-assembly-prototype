import { AgreementDocumentType, AgreementType } from './AgreementType';

export type ExportAgreementRequestBody = {
  loanConfig: {
    loanId: String;
  } & Record<string, unknown>;
  agreement: AgreementType;
};

export type GetAgreementRequestBody = Record<string, unknown>;
