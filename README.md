# Demo Project to demonstrate concept of document assembly

## How to start

### Start the Spring Boot Application

_Prerequisites_

You need to have Java Version 11 installed.

_Build the application_

Before you start the Spring Boot Application for the first time you need to run the build script first. To do so use the
following command in the `kotlin-poc` folder

```console
./gradlew build
```

_Run the application_

In the `kotlin-poc` folder run the following command to run the application:

```console
./gradlew bootRun
```

### Start the serverless backend

Navigate into the `serverless` folder. Then start the serverless backend with the following command:

```console
serverless offline start
```

The subproject `serverless` has a readme on its own. Read it to learn more about the config and what the command does.

### Start the react-website

Navigatae into the `react-website` folder. Run it with the following commad:

```console
yarn start
```

### Start the Word-Add-In

Navigate into the `word-add-in` folder. Run it with the following command:

```console
yarn start
```

This will open a Microsoft Word Document and in the menu bar in the top a ican that says `Logo` with the text
`Show Taskpane` underneath should appear. Click on it to open the sidepanel and use the add-in

**Shut the Add-In down**

To stop the webserver to serve the add-in type the following command in the `word-add-in` folder:

```console
yarn stop
```
