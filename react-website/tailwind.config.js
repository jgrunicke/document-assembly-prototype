module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    colors: {
      'dark-blue': '#00205b',
      'light-blue': '#5a6f9a',
      'light-gray': '#edf0f2',
      gray: '#bfbfc1',
      'dark-gray': '#9c9d9e',
      white: '#ffffff',
    },
    fontFamily: {
      sans: ['Nunito', 'sans-serif'],
    },

    extend: {},
  },
  plugins: [],
};
