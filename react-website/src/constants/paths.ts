// Paths to different backend endpoints

export const docxServicePath = 'http://localhost:8080/document-assembly';
export const serverlessPath = 'http://localhost:3000/document-assembly';

export const exportAgreementEndpoint = `${docxServicePath}/print`;
export const importAgreementEndpoint = `${docxServicePath}/parse`;
export const loadStyleTemplateEndpoint = `${docxServicePath}/style-template`;
export const parseTemplateEndpoint = `${docxServicePath}/template`;

export const serverlessAgreementEndpoint = '/agreement';
export const serverlessTemplateEndpoint = '/template';
