import { AgreementType } from '../types/AgreementType';

/**
 * Finds the newest version out of an array of different versions of an agreement.
 * @param agreements List of agreements
 * @returns The agreement object with the newest version.
 */
export const getNewestVersion = (agreements: AgreementType[]) =>
  [...agreements].sort((a, b) => a.version - b.version).pop();
