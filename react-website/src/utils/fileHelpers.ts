/**
 * Launches the download in the browser of a file
 * @param fileName Name of the downloaded file.
 * @param base64 The file to be downloaded decoded as a base64 string.
 */
export const launchDownload = async (fileName: string, base64: string) => {
  const mimeType = 'application/octet-stream';
  const response = await fetch(`data:${mimeType};base64,${base64}`);
  const blob = await response.blob();
  var link = window.document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  link.download = fileName;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};
