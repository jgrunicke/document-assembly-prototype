import { Link } from 'react-router-dom';

interface Props {}

/**
 * Displays the navbar to navigate between the loan negation and the template
 *  administration.
 */
export const Navbar: React.FC<Props> = props => {
  return (
    <div>
      <nav className="w-screen pt-6 px-10">
        <div className="flex gap-x-10 justify-start items-center">
          <div className="text-xl font-bold text-dark-blue">Realstocks</div>
          <div className="flex items-center gap-x-7">
            <Link to="/" className="text-sm text-dark-gray">
              LOAN NEGOTIATION
            </Link>
            <Link to="/templates" className="text-sm text-dark-gray">
              TEMPLATE ADMIN
            </Link>
          </div>
        </div>
      </nav>
    </div>
  );
};
