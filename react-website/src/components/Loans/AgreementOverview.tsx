import { AgreementType } from '../../types/AgreementType';
import { LoanConfig } from '../../types/loanConfigType';
import { AgreementOpertationsBar } from './AgreementOptionsBar';
import { AgreementVersionsList } from './AgreementVersionsList';

/**
 * The functions that can be called in the operations bar.
 */
export interface Operations {
  handleExport: () => Promise<void>;
  handleImport: (file: File) => Promise<void>;
}

interface Props {
  props: {
    agreementVersions: AgreementType[];
    options: Operations;
  };
}

/**
 * Parent component that hold the operations bar and the agreement version list components.
 * @param param0 The agreement versions and the Operations that get passed to the child components.
 * @returns React component.
 */
export const AgreementOverview: React.FC<Props> = ({ props: { agreementVersions, options } }) => {
  return (
    <div>
      <h1 className="text-xl grow font-bold"> Agreement Overview</h1>
      <div>
        <AgreementOpertationsBar options={options} />
        <AgreementVersionsList agreements={agreementVersions} />
      </div>
    </div>
  );
};
