import React, { useState } from 'react';
import { Operations } from './AgreementOverview';
import Modal from 'react-modal';
interface Props {
  options: Operations;
}

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

/**
 * Operations bar component that provides the user the functionality to perform
 *  operations on the agreements like exporting and importing a new version.
 * @param param0 The options are the functions that handle the file import or export.
 * @returns React component.
 */
export const AgreementOpertationsBar: React.FC<Props> = ({ options }) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [file, setFile] = useState<File | undefined>(undefined);

  const closeModal = () => {
    setModalOpen(false);
  };

  const openModal = () => {
    setModalOpen(true);
  };

  const onFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const uploadedFile = event.target.files![0];
    setFile(uploadedFile);
  };

  const onFileUpload = async () => {
    if (file === undefined) return;
    options.handleImport(file);
  };

  return (
    <div className="py-4">
      <span className="pr-2">
        <button className="bg-light-blue text-white py-1 px-4 rounded-md" onClick={() => options.handleExport()}>
          Export Agreement
        </button>
      </span>
      <span>
        <button className="bg-light-blue text-white py-1 px-4 rounded-md" onClick={() => openModal()}>
          Import Agreement
        </button>
      </span>
      <Modal isOpen={modalOpen} onRequestClose={closeModal} style={customStyles} contentLabel="Import Agreement">
        <h2 className="text-xl grow font-bold">Import Agreement</h2>
        <div>
          <label>Select File to upload changes</label>
          <input type="file" onChange={event => onFileChange(event)} />
        </div>
        <div className="py-4">
          <button
            className="bg-light-blue disabled:bg-gray text-white py-1 px-4 rounded-md mr-2"
            disabled={file === undefined}
            onClick={() => onFileUpload()}
          >
            Upload File
          </button>
          <button
            className=" text-light-blue border border-ligh-blue py-1 px-4 rounded-md"
            onClick={() => closeModal()}
          >
            Close Modal
          </button>
        </div>
        <div></div>
      </Modal>
    </div>
  );
};
