import { Field, Form, Formik, FormikHelpers } from 'formik';
import {
  BaseRate,
  DayCountConvention,
  HolidayCalendar,
  InterestPeriod,
  InterestType,
  LoanConfig,
} from '../../types/loanConfigType';
import { InputField, SelectField } from '../shared/Forms';

interface Props {
  initialValues: LoanConfig;
  handleSubmit: (values: LoanConfig, actions: FormikHelpers<LoanConfig>) => Promise<void>;
}

/**
 * Displays the input fields to configure a loan.
 * @param param0 The initial values of the input fields and the function that handles the submit event.
 * @returns React component.
 */
export const LoanNegotiation: React.FC<Props> = ({ initialValues, handleSubmit }) => {
  return (
    <div>
      <h1 className="text-xl grow font-bold"> Loan Configuration</h1>
      <div className="py-6">
        <Formik initialValues={initialValues} onSubmit={(values, actions) => handleSubmit(values, actions)}>
          <Form>
            <h2 className="text-lg font-semibold">Basic information</h2>
            <div className="flex justify-center gap-2 max-w-md flex-col">
              <InputField label="Loan ID" propertyName="loanId"></InputField>
              <InputField label="Purpose" propertyName="purpose"></InputField>
            </div>

            <h2 className="text-lg font-semibold pt-8 pb-4">Interest</h2>
            <div className="flex justify-center gap-2 max-w-md flex-col">
              <SelectField
                label="Interest Type"
                propertyName="interestType"
                options={[
                  {
                    label: '',
                    value: InterestType.UNSET,
                  },
                  {
                    label: 'Floating',
                    value: InterestType.Floating,
                  },
                  {
                    label: 'Fixed',
                    value: InterestType.Fixed,
                  },
                ]}
              />
              <InputField label="FixedRate (%)" propertyName="interestFixedRate"></InputField>
              <SelectField
                label="Base Rate"
                propertyName="interestBaseRate"
                options={[
                  {
                    label: '',
                    value: BaseRate.UNSET,
                  },
                  {
                    label: 'EURIBOR',
                    value: BaseRate.EURIBOR,
                  },
                  {
                    label: 'LIBOR',
                    value: BaseRate.LIBOR,
                  },
                  {
                    label: 'STIBOR',
                    value: BaseRate.STIBOR,
                  },
                  {
                    label: 'NIBOR',
                    value: BaseRate.NIBOR,
                  },
                ]}
              />
              <InputField label="Margin (%)" propertyName="interestMargin" />
              <InputField label="QuotationDate" propertyName="interestQuotationDate" />
            </div>
            <h2 className="text-lg pt-6 pb-4 font-semibold">Interest Period</h2>
            <div className="flex justify-center gap-2 max-w-md flex-col">
              <SelectField
                label="Interest Period"
                propertyName="interestPeriod"
                options={[
                  {
                    label: '',
                    value: '',
                  },
                  {
                    label: 'Monthly',
                    value: InterestPeriod.MONTHLY,
                  },
                  {
                    label: 'Quarterly',
                    value: InterestPeriod.QUARTERLY,
                  },
                  {
                    label: 'Half-Yearly',
                    value: InterestPeriod.HALF_YEARLY,
                  },
                  {
                    label: 'Yearly',
                    value: InterestPeriod.YEARLY,
                  },
                ]}
              />
              <SelectField
                label="Holiday Calendar"
                propertyName="interestHolidayCalendar"
                options={[
                  {
                    label: '',
                    value: HolidayCalendar.UNSET,
                  },
                  {
                    label: HolidayCalendar.DENMARK,
                    value: HolidayCalendar.DENMARK,
                  },
                  {
                    label: HolidayCalendar.FINNLAND,
                    value: HolidayCalendar.FINNLAND,
                  },
                  {
                    label: HolidayCalendar.SWEDEN,
                    value: HolidayCalendar.SWEDEN,
                  },
                  {
                    label: HolidayCalendar.NORWAY,
                    value: HolidayCalendar.NORWAY,
                  },
                ]}
              />
            </div>
            <div className="py-8 flex justify-center max-w-sm">
              <button className="bg-light-blue text-white py-1 px-4 rounded-md" type="submit">
                Submit
              </button>
            </div>
          </Form>
        </Formik>
      </div>
    </div>
  );
};
