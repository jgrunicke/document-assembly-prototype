import { AgreementType } from '../../types/AgreementType';
interface Props {
  agreement: AgreementType;
}

/**
 * Display one agreement version.
 *  This sould be subsituted later with the real editor on the plattform.
 * @param param0 The agreement object that should be displayed.
 * @returns React component.
 */
export const AgreementViewComponent: React.FC<Props> = ({ agreement }) => {
  return (
    <div className="py-4">
      <div>
        <h3 className="text-l grow font-bold">Version {agreement.version}</h3>
      </div>
      <div>{JSON.stringify(agreement, null, '\n')}</div>
    </div>
  );
};
