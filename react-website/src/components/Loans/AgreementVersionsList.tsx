import { AgreementType } from '../../types/AgreementType';
import { AgreementViewComponent } from './AgreementView';

interface Props {
  agreements: AgreementType[];
}

/**
 * Display the list of agreement versions.
 * @param param0 The list of agreements to be displayed.
 * @returns  React component.
 */
export const AgreementVersionsList: React.FC<Props> = ({ agreements }) => {
  return (
    <div>
      {agreements.map(agreement => (
        <AgreementViewComponent key={agreement.version} agreement={agreement} />
      ))}
    </div>
  );
};
