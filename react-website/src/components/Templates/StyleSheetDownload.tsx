import { loadStyleSheet } from '../../api/docx-api';
import { launchDownload } from '../../utils/fileHelpers';

interface Props {}

/**
 * User Interface to download the style sheet to create a new template.
 * @returns
 */
export const StyleSheetDownload: React.FC<Props> = () => {
  const handleDownload = async () => {
    const file = await loadStyleSheet();

    await launchDownload('RS-Template-Stylesheet.docx', file.base64);
  };

  return (
    <div>
      <h2 className="pt-4 text-lg">Download Style Sheet</h2>
      <div className="flex py-2 max-w-sm justify-center items-center">
        <button onClick={handleDownload} className="text-white bg-light-blue rounded-md py-1 px-4 text-sm">
          DOWNLOAD STYLESHEET
        </button>
      </div>
    </div>
  );
};
