import { Field, Form, Formik, FormikHelpers } from 'formik';
import { useState } from 'react';
import { createTemplateTail } from 'typescript';
import { parseTempalte } from '../../api/docx-api';
import { createTemplate } from '../../api/serverless-api';
import { TemplateType } from '../../types/TemplateType';
import { InputField } from '../shared/Forms';

interface Props {}

interface State {
  file: File;
}

interface TemplateConfig {
  purpose?: string;
  lender?: string;
}

/**
 * Component to upload a new template. Handles the uploaded file by sending it first to the
 *  template parser and later to the serverless endpoint to save the new template.
 * @returns
 */
export const UploadTemplate: React.FC<Props> = () => {
  const [state, setState] = useState<State>();

  // Handles file upload
  const onFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files![0];
    setState({ file });
  };

  const initialValues: TemplateConfig = {
    purpose: '',
    lender: '',
  };

  // Handles submit of the uploaded file.
  const onSubmit = async (values: TemplateConfig, actions: FormikHelpers<any>) => {
    const file = state?.file;
    if (file === undefined) {
      console.log('No file selected');
      return;
    }
    const formData = new FormData();
    formData.append('file', file, file.name);

    // Call Template parser to create temlate object.
    const parsedTemplate = await parseTempalte(formData);

    const template: TemplateType = {
      properties: {
        purpose: values.purpose ? values.purpose : undefined,
        lender: values.lender ? values.lender : undefined,
      },
      document: parsedTemplate,
    };

    // Call the serverless backend to save the parsed template.
    await createTemplate(template);

    actions.setSubmitting(false);
    console.log('Template successfully created');
  };

  return (
    <div className="flex max-w-sm items-center">
      <div className="flex flex-col">
        <Formik initialValues={initialValues} onSubmit={(values, actions) => onSubmit(values, actions)}>
          <Form>
            <div className="flex justify-center gap-2 max-w-md flex-col">
              <InputField propertyName="purpose" label="Purpose" />
              <InputField propertyName="lender" label="Lender" />
              <div></div>
              <div className="flex gap-6 items-center justify-end">
                <label className="">File</label>
                <div className="w-7/12">
                  <input className="" type="file" onChange={event => onFileChange(event)} />
                </div>
              </div>
              <div></div>
            </div>
            <div className="flex justify-center pt-6">
              <button
                className="bg-light-blue disabled:bg-gray text-white py-1 px-4 text-sm rounded-md mr-2"
                disabled={state?.file === undefined}
                type="submit"
              >
                CREATE TEMPLATE
              </button>
            </div>
          </Form>
        </Formik>
      </div>
    </div>
  );
};
