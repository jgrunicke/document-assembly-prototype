import { Field } from 'formik';
import { InterestType } from '../../types/loanConfigType';

// Forms that can be used at several places. (For the prototype only used in the loan negoation).
interface Props {
  label: string;
  propertyName: string;
}

/**
 * Generic input field.
 * @param param0 The name of the property that should be bound to the input field and the name of the label.
 * @returns
 */
export const InputField: React.FC<Props> = ({ label, propertyName }) => {
  return (
    <div className="flex gap-6 justify-end items-center">
      <label htmlFor={propertyName}>{label}</label>
      <Field name={propertyName} className="rounded-md w-7/12 text-right p-1 pr-4 bg-light-gray"></Field>
    </div>
  );
};

interface SelectFieldProps {
  label: string;
  propertyName: string;
  options: Option[];
}

export interface Option {
  label: string;
  value: string;
}

/**
 * A generic component for a select field.
 * @param param0 The name of the property that should be bound to
 *  the select field, the label and the possible options too choose from.
 * @returns
 */
export const SelectField: React.FC<SelectFieldProps> = ({ label, propertyName, options }) => {
  return (
    <div className="flex gap-6 justify-end items-center">
      <label htmlFor={propertyName}>{label}</label>
      <Field as="select" name={propertyName} className="w-7/12 rounded-md text-right p-1 pr-4 bg-light-gray">
        {options.map(option => {
          return (
            <option key={option.label} value={option.value}>
              {option.label}
            </option>
          );
        })}
      </Field>
    </div>
  );
};
