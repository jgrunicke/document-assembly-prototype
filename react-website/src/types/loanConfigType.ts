export type LoanConfig = {
  loanId: string;
  purpose: string;
  lenderName: string;
  borrowerName: string;
  interestType: InterestType;
  interestFixedRate: number;
  interestBaseRate: BaseRate;
  interestMargin: number;
  interestQuotationDate: string;
  interestHolidayCalendar: HolidayCalendar;
  interestDayCountConvention: DayCountConvention | '';
  interestPeriod: InterestPeriod | '';
};

export enum InterestPeriod {
  MONTHLY = '1',
  QUARTERLY = '3',
  HALF_YEARLY = '6',
  YEARLY = '12',
}

export enum DayCountConvention {
  ACTUAL_360 = 'Actual/360',
  ACTUAL_365 = 'Actual/365',
  ACTUAL_ACTUAL = 'Actual/Actual',
  THIRTY_360 = '30/360',
  THIRTY_365 = '30/365',
}

export enum HolidayCalendar {
  UNSET = '',
  SWEDEN = 'Sweden',
  NORWAY = 'Norway',
  FINNLAND = 'Finland',
  DENMARK = 'Denmark',
}

export enum InterestType {
  UNSET = '',
  Fixed = 'fixed',
  Floating = 'floating',
}

export enum BaseRate {
  UNSET = '',
  EURIBOR = 'EURIBOR',
  STIBOR = 'STIBOR',
  NIBOR = 'NIBOR',
  LIBOR = 'LIBOR',
}
