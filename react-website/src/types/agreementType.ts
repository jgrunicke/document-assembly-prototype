import { RulesLogic } from 'json-logic-js';

export type AgreementType = {
  loanId: string;
  version: number;
  properties: Record<string, unknown>;

  document: AgreementDocumentType;
};

export type AgreementDocumentType = {
  type: 'agreement_document';
  preamble: AgreementAnyDocType[];
  sections: AgreementSectionType[];

  // Map from reference ID of parent comment to comment chain. Only chains that
  // have not been attached to edit doc elements are included in this map.
  commentChainMap?: Record<number, AgreementCommentChainType>;
};

export type AgreementSectionType = {
  type: 'section';
  sectionNumber: string;
  heading: AgreementHeadingType;
  preamble: AgreementAnyDocType[];
  subsections: AgreementSubsectionType[];
  conditionRule?: RulesLogic;
};

export type AgreementSubsectionType = {
  type: 'subsection';
  sectionNumber: string;
  heading: AgreementHeadingType;
  content: AgreementAnyDocType[];
  mergeTag?: AgreementMergeTagType;
  conditionRule?: RulesLogic;
};

export type AgreementAnyDocType = AgreementEditDocType | AgreementViewDocType;

export type AgreementEditDocType = {
  type: 'edit_doc';
  title: string;
  doc: AgreementDocType;
  commentChains?: AgreementCommentChainType[];
  listContext?: AgreementListContextType;
  conditionRule?: RulesLogic;
};

export type AgreementListContextType = {
  type: 'list_context';
  numId: number;
  start: number;
  // Map from edit ID of previous editable list to the initial length
  // of the list contained within that region.
  listLengthMap?: Record<string, number>;
};

export type AgreementCommentChainType = {
  type: 'comment_chain';
  comments: AgreementCommentType[];
};

export type AgreementCommentType = {
  type: 'comment';
  durableId: string;
  time: string;
  done: boolean;
  content: AgreementDocType;
};

export type AgreementViewDocType = {
  type: 'view_doc';
  title: string;
  doc: AgreementDocType;
  conditionRule?: RulesLogic;
};

export type AgreementHeadingType = {
  type: 'heading';
  title: string;
  level: number;
  bookmarks: Record<string, true>; // Set of bookmark names
};

export type AgreementDocType = {
  type: 'doc';
  content: AgreementBlockType[];
};

export type AgreementParagraphType = {
  type: 'paragraph';
  content?: AgreementParagraphContentType[];
};

export type AgreementParagraphContentType = AgreementTextType | AgreementMergeTagType;

export type AgreementTextType = {
  type: 'text';
  text: string;
  marks?: AgreementMarkType[];
};

export type AgreementMarkType = {
  type: 'em' | 'strong';
};

export type AgreementListType = {
  type: 'ordered_list';
  listContext?: AgreementListContextType;
  content: AgreementBlockType[];
};

export type AgreementBlockType = AgreementParagraphType | AgreementListType;

// Additional comment details that are not available in the OOXML format.
export type AgreementCommentMetaType = {
  sub?: string; // DEPRECATED
  userId: string; // The user that created the comment.
  companyId: string; // The company on behalf of which the comment was created.
  createdAt: string; // Creation time of the comment.
};

export type AgreementMergeTagType = {
  type: 'merge_tag';
  tag: string;
  query: RulesLogic;
};
