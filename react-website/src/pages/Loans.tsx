import { FormikHelpers } from 'formik';
import { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { exportAgreement, importAgreement } from '../api/docx-api';
import { loadAgreement, updateAgreement } from '../api/serverless-api';
import { AppState } from '../App';
import { AgreementOverview } from '../components/Loans/AgreementOverview';
import { LoanNegotiation } from '../components/Loans/LoanNegotation';
import { Navbar } from '../components/Navbar';
import { AgreementType } from '../types/AgreementType';
import { LoanConfig, InterestType, BaseRate, HolidayCalendar } from '../types/loanConfigType';
import { launchDownload } from '../utils/fileHelpers';
import { getNewestVersion } from '../utils/loanHelpters';

export interface State {
  loanConfig: LoanConfig;
  agreementVersions: AgreementType[];
}

/**
 * The page to display the loan configuration and the agreements.
 * @returns
 */
export const Loans: React.FC = () => {
  const initialState: AppState = {
    loanConfig: {
      loanId: '2',
      purpose: 'test',

      lenderName: 'TetsLender',
      borrowerName: 'Testborrower',
      interestType: InterestType.UNSET,
      interestFixedRate: 0,
      interestBaseRate: BaseRate.UNSET,
      interestMargin: 0,
      interestQuotationDate: '',
      interestDayCountConvention: '',
      interestPeriod: '',
      interestHolidayCalendar: HolidayCalendar.UNSET,
    },
    agreementVersions: [],
  };

  const [appState, setAppState] = useState<AppState>(initialState);

  /**
   * Handles submit of the loan configurations. Calls the backend to load the agreement,
   *  create a new one if none existing and swap out the template if not fitting anymore
   *  to the current configurations.
   *
   *  Function gets passed to child component.
   * @param values The current values of the input fields.
   * @param actions helper functions to control the Formik behaviour.
   */
  const handleSubmit = async (values: LoanConfig, actions: FormikHelpers<LoanConfig>) => {
    console.log({ values });

    const agreementVersions = await loadAgreement(values);

    setAppState({ ...appState, agreementVersions, loanConfig: values });
    actions.setSubmitting(false);
  };

  /**
   * Handles the export of the latest version of the agreement object by
   *  calling the word printer to create a Word file.
   *  After that the file gets downloaded by the user.
   *
   *  Function gets passed to child components.
   */
  const handleExport = async () => {
    const { loanConfig, agreementVersions } = appState;

    const newestVersion = getNewestVersion(agreementVersions);
    if (newestVersion === undefined) throw new Error('No Agreement Loaded');

    const file = await exportAgreement({ agreement: newestVersion, loanConfig });

    await launchDownload('fa-test.docx', file.base64);
  };

  /**
   * Handles the import of a new version of an agreement.
   *
   * The functions calls the Word parser to convert the uploaded Word file into an
   *  agreement object.
   *
   * Afterwards it calls the serverless backend to update the agreement and create a new version.
   * @param file The updated Word file with the changes to the agreement.
   */
  const handleImport = async (file: File) => {
    const { agreementVersions } = appState;
    const agreement = getNewestVersion(agreementVersions);
    if (agreement === undefined) throw new Error('No Agreement Loaded');

    console.log(agreement);
    const json = JSON.stringify(agreement.document);
    const blob = new Blob([json], {
      type: 'application/json',
    });

    const formData = new FormData();

    formData.append('file', file, file?.name);
    formData.append('document', blob);

    const importedDocument = await importAgreement(formData);

    const newVersion = await updateAgreement(agreement.loanId, importedDocument);
    agreementVersions.push(newVersion);

    setAppState({
      ...appState,
      agreementVersions,
    });
  };

  return (
    <div className="">
      <LoanNegotiation initialValues={appState.loanConfig} handleSubmit={handleSubmit} />
      {appState.agreementVersions ? (
        <AgreementOverview
          props={{
            agreementVersions: appState.agreementVersions!,
            options: {
              handleExport,
              handleImport,
            },
          }}
        />
      ) : null}
    </div>
  );
};
