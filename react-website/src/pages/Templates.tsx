import { StyleSheetDownload } from '../components/Templates/StyleSheetDownload';
import { UploadTemplate } from '../components/Templates/UploadTemplate';

interface State {}

/**
 * Page to administrate the templates.
 *
 * This page holds the functionalities:
 *  - download a stylesheet to create a new template
 *  - upload a newly created template
 */
export const Templates: React.FC = () => {
  return (
    <div>
      <h1 className="text-xl">Template Administration</h1>
      <StyleSheetDownload />
      <h2 className="pt-10 pb-4 text-lg">Upload new Template</h2>
      <UploadTemplate />
    </div>
  );
};
