import React, { useState } from 'react';
import Modal from 'react-modal';
import { FormikHelpers } from 'formik';
import { AgreementType } from './types/AgreementType';
import { loadAgreement, updateAgreement } from './api/serverless-api';
import { exportAgreement, importAgreement } from './api/docx-api';
import { BaseRate, InterestType, LoanConfig } from './types/loanConfigType';
import { launchDownload } from './utils/fileHelpers';
import { AgreementOverview } from './components/Loans/AgreementOverview';
import { LoanNegotiation } from './components/Loans/LoanNegotation';
import { Navbar } from './components/Navbar';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { getNewestVersion } from './utils/loanHelpters';
import { Loans } from './pages/Loans';
import { Templates } from './pages/Templates';

export type AppState = {
  loanConfig: LoanConfig;
  agreementVersions: AgreementType[];
};

Modal.setAppElement('#root');
const App: React.FC = () => {
  return (
    <Router>
      <div>
        <Navbar />
        <div className="px-20 py-10">
          <Routes>
            <Route path="/templates" element={<Templates />}></Route>
            <Route path="/" element={<Loans />}></Route>
          </Routes>
        </div>
      </div>
    </Router>
  );
};

export default App;
