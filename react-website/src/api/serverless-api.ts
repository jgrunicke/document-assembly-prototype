import { LoanConfig } from '../types/loanConfigType';
import axios from 'axios';
import { serverlessAgreementEndpoint, serverlessPath, serverlessTemplateEndpoint } from '../constants/paths';
import { AgreementDocumentType, AgreementType } from '../types/AgreementType';
import { TemplateType } from '../types/TemplateType';

// API to call serverless functions

/**
 * Loads agreement object for the a configured loan. The backend creates a new agreement object, if none exists
 *  yet. If the template does not suit the current configurations anymore the backend swaps out to the fitting templte.
 * @param loanConfig The current configuration of the loan.
 * @returns Array of all agreement objects (versions) for the loan.
 */
export const loadAgreement = async (loanConfig: LoanConfig) => {
  const response = await axios.post(`${serverlessPath}${serverlessAgreementEndpoint}/${loanConfig.loanId}`, loanConfig);

  const agreements: AgreementType[] = response.data;

  return agreements;
};

/**
 * Calls backend to update the agreement object for a specific loan.
 * @param loanId The id of the to be updated loan.
 * @param document The to be updated agreement object of the loan with the changes.
 * @returns The updated version of the agreement object.
 */
export const updateAgreement = async (loanId: string, document: AgreementDocumentType) => {
  const response = await axios.put(`${serverlessPath}${serverlessAgreementEndpoint}/${loanId}`, document);

  const agreement: AgreementType = response.data;

  return agreement;
};

/**
 * Calls the backend to save a new template.
 * @param template The template object that should be saved.
 */
export const createTemplate = async (template: TemplateType) => {
  const response = await axios.post(`${serverlessPath}${serverlessTemplateEndpoint}`, template);

  console.log(`createTemplate(): response=${JSON.stringify(response, null, 2)}`);
};
