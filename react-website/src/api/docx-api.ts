import axios from 'axios';
import process from 'process';
import { LoanConfig } from '../types/loanConfigType';
import {
  exportAgreementEndpoint,
  importAgreementEndpoint,
  loadStyleTemplateEndpoint,
  parseTemplateEndpoint,
} from '../constants/paths';
import { AgreementDocumentType, AgreementType } from '../types/AgreementType';

// API for the Word parser and printer.

/**
 * Function to call the Word printer and to create a Word document out of the current version of the
 *  agreement object and the current loan configuration. Printer needs the configuration to evaluate
 *  control elements (merge tags, conditions).
 * @param params Parameter object for the printer consisting  of the current
 *  loan configuration and the agreement object
 * @returns The created Word file.
 */
export const exportAgreement = async (params: {
  loanConfig: LoanConfig;
  agreement: AgreementType;
}): Promise<{ base64: string }> => {
  const response = await axios.post(exportAgreementEndpoint, params);

  return response.data;
};

/**
 * Calls the Word parser to read the uploaded word file and transform in into a agreement object.
 * @param formData The formdata object holds the uploaded Word file as property `file`.
 * @returns The parsed document as an agreement object that the Word parser returns.
 */
export const importAgreement = async (formData: FormData) => {
  const response = await axios.post(importAgreementEndpoint, formData);
  console.log(JSON.stringify(response, null, 2));

  const document: AgreementDocumentType = response.data;

  return document;
};

/**
 * Loads the style template from the spring applicataion which serves as a foundation to create a new
 *  template.
 * @returns The Word file that is used as a style template to create a new template.
 */
export const loadStyleSheet = async (): Promise<{ base64: string }> => {
  const response = await axios.get(loadStyleTemplateEndpoint);

  return response.data;
};

/**
 * Calls the template parser to convert a Word file into a template object.
 * @param formData The formdata object holds the uploaded Word file.
 * @returns The parsed template as template object.
 */
export const parseTempalte = async (formData: FormData) => {
  const response = await axios.post(parseTemplateEndpoint, formData);
  console.log(JSON.stringify(response, null, 2));

  const document: AgreementDocumentType = response.data;

  return document;
};
